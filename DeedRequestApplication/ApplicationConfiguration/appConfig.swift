//
//  appConfig.swift
//  DeedRequestApplication
//
//  Created by admin on 7/5/2563 BE.
//  Copyright © 2563 EarthBlooProduction. All rights reserved.
//

import Foundation

class appConfig {
    let url = "xx"
    static let shared = appConfig()
    var con:Config!
    func fetchConfig(user_id:Int){
        
        var semaphore = DispatchSemaphore (value: 0)

        var request = URLRequest(url: URL(string: "http://land-surveyor-dev.eba-atp2ae9g.ap-southeast-1.elasticbeanstalk.com/api/v1/configs?user_id=\(user_id)")!,timeoutInterval: Double.infinity)
        request.httpMethod = "GET"

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
          guard let data = data else {
            print(String(describing: error))
            return
          }
          do{
                  let decoder = JSONDecoder()
                  self.con =  try decoder.decode(Config.self, from: data)
                    print(self.con)
          
              }catch{
                  print(String(describing: error))
                  
              }
            semaphore.signal()
          }

        task.resume()
        semaphore.wait()
       
        
    }
    
    
}

struct Config:Decodable {
    var app_name:String!
    var user:UserCon!
    var resources:Resource!
    var maximum_file_image:Int!
    var maximum_image_size:Int!
    var allowed_image_formats: [String]!
}

struct UserCon:Decodable{
    var customer_id:Int!
    var user_id:Int!
    var first_name:String!
    var last_name:String!
    var phone_number:String!
    
}

struct Resource:Decodable{
    var deeds:Link!
    var status:[StatusCon]!
}
struct Link:Decodable{
    var url:String!
}

struct StatusCon:Decodable {
    var status_id:Int!
    var name:String!
}


