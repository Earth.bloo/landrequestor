//
//  Form2ViewController.swift
//  DeedRequestApplication
//
//  Created by admin on 20/4/2563 BE.
//  Copyright © 2563 EarthBlooProduction. All rights reserved.
//

import UIKit
import ArcGIS
import MaterialComponents.MaterialTextFields
import MaterialComponents.MaterialTextControls_OutlinedTextAreas
import TLPhotoPicker
import Lottie


class Form2ViewController: UIViewController,AGSGeoViewTouchDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,TLPhotosPickerViewControllerDelegate,UITextFieldDelegate {
    var selectedImage:[UIImage] = []
    var selectedAssets = [TLPHAsset]()
    var mediaAssets:[Media] = []
    var MultiimagePickerController:TLPhotosPickerViewController!
    var point = AGSPoint(x: 0, y: 0, spatialReference: .webMercator())
    var mapForm:mapFormVIewController!
    var presenter:FormPresenter!
    
    let locationButton:UIButton = {
        let view = UIButton()
        view.backgroundColor = UIColor.gray
        view.setTitle("Select Location", for: UIControl.State.normal)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.titleLabel?.font = UIFont(name: "AirbnbCerealApp-Book",size: 25)
        view.addTarget(self, action: #selector(clickLocation), for: .touchUpInside)
        
        return view
    }()
    
    let mapView:AGSMapView = {
        let view = AGSMapView()
        //view.backgroundColor = UIColor.green
        return view
    }()
     private var graphicsOverlay:AGSGraphicsOverlay!
    
    //upload latitude and longitude
       var lat:String!
       var long:String!
       
    
    let scrollView:UIScrollView = {
        let view = UIScrollView()
        view.contentSize.height = 1000
        view.backgroundColor = UIColor.white
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var DeedNameTextField:MDCTextField! = {
           let view = MDCTextField()
           view.placeholder = "DEED NAME"
           view.font = UIFont(name: "AirbnbCerealApp-Book", size: 18)
           view.translatesAutoresizingMaskIntoConstraints = false
           return view
       }()
    var textcontroller1:MDCTextInputControllerOutlined!
    
    var AddressTextField:MDCMultilineTextField! = {
        let view = MDCMultilineTextField()
        view.placeholder = "ADDRESS"
        view.font = UIFont(name: "AirbnbCerealApp-Book", size: 18)
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
        
    }()
    var textcontroller2:MDCTextInputControllerOutlinedTextArea!
   
    
    var LandmarkTextField:MDCTextField = {
              let view = MDCTextField()
              view.placeholder = "NEARBY LANDMARK"
              view.font = UIFont(name: "AirbnbCerealApp-Book", size: 18)
              view.translatesAutoresizingMaskIntoConstraints = false
              return view
          }()
    var textcontroller3:MDCTextInputControllerOutlined!
    
    var photoButton:UIButton = {
        let view = UIButton()
        view.backgroundColor = UIColor.lightGray
        view.setTitle("Add Location Photo", for: UIControl.State.normal)
        view.titleLabel?.font = UIFont(name: "AirbnbCerealApp-Book",size: 17)
        view.layer.cornerRadius = 25
        view.addTarget(self, action: #selector(upoadphoto), for: .touchUpInside)
        return view
    }()
    fileprivate let PhotoCollectionView:UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        
        layout.minimumLineSpacing = 5
        //layout.minimumInteritemSpacing = 0
        layout.scrollDirection = .horizontal
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        layout.itemSize = CGSize(width: 84, height: 84)
        cv.backgroundColor = UIColor.white
        let bgimage = UIImageView()
        bgimage.contentMode = .scaleAspectFit
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.register(ImageCollectionCell.self, forCellWithReuseIdentifier: "Cell")
        //cv.isPagingEnabled = true
        return cv
    }()
    
    var sumbitButton:UIButton = {
         let view = UIButton()
         view.layer.borderWidth = 1
         view.layer.borderColor = colour.deepblue.cgColor
         view.layer.cornerRadius = 20
         view.setTitle("Submit", for: UIControl.State.normal)
         view.translatesAutoresizingMaskIntoConstraints = false
         view.titleLabel?.font = UIFont(name: "Prompt-Light",size: 18)
        view.setTitleColor(colour.deepblue, for: UIControl.State.normal)
         
         view.addTarget(self, action: #selector(submitbutton), for: .touchUpInside)
         return view
    }()
    
    var sumbitButton2:UIButton = {
            let view = UIButton()
            view.layer.borderWidth = 1
            view.layer.borderColor = colour.deepblue.cgColor
            view.layer.cornerRadius = 20
            view.setTitle("Submit", for: UIControl.State.normal)
            view.translatesAutoresizingMaskIntoConstraints = false
            view.titleLabel?.font = UIFont(name: "Prompt-Light",size: 18)
           view.setTitleColor(colour.deepblue, for: UIControl.State.normal)
            
            view.addTarget(self, action: #selector(submitbutton), for: .touchUpInside)
            return view
       }()
    
    let loader:UIActivityIndicatorView = {
        let loader = UIActivityIndicatorView()
        loader.style = .large
        return loader
    }()
    
    
    //LOADER PAGE
    
    let bgloader:UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        return view
    }()
    
    var animatedView:AnimationView = {
        let view = AnimationView()
        view.animation = Animation.named("mapmove")
        view.contentMode = .scaleAspectFill
        view.loopMode = .loop
        
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let loaderText:UILabel = {
        let view = UILabel()
        view.text = "Loading"
        view.font = UIFont(name: "AirbnbCerealApp-Book", size: 22)
        return view
    }()
    
    
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
        mapForm = mapFormVIewController()
               DeedNameTextField.becomeFirstResponder()
                AddressTextField.becomeFirstResponder()
                LandmarkTextField.becomeFirstResponder()
                
          presenter = FormPresenter()
          mapView.touchDelegate = self
          presenter.delegate = self
          MultiimagePickerController = TLPhotosPickerViewController()
          MultiimagePickerController.delegate = self
          PhotoCollectionView.delegate = self
          PhotoCollectionView.dataSource = self
          
         PhotoCollectionView.register(UINib(nibName: "PhotoCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "PhotoCollectionViewCell")
         
            graphicsOverlay = AGSGraphicsOverlay()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
         
         //navigation Bar
         let titlelabel = UILabel(frame: CGRect(x: 0, y: 0, width: view.frame.width , height: view.frame.height))
         titlelabel.text  = "Add Request"
         
         titlelabel.font = UIFont(name:"AirbnbCerealApp-Bold",size: 23)
         titlelabel.textColor = UIColor.white
         self.tabBarController?.navigationItem.titleView = titlelabel
     }
    
    
    func setUI(){
        view.addSubview(scrollView)
        scrollView.setConstraint(top: view.topAnchor, leading: view.leadingAnchor, trailing: view.trailingAnchor, bottom: view.bottomAnchor)
        
        scrollView.addSubview(mapView)
               mapView.setConstraint(top: scrollView.topAnchor, leading: view.leadingAnchor, trailing: view.trailingAnchor, bottom: nil, padding: .init(top: 20, left: 20, bottom: 20, right: 20), size: .init(width: 0, height: 250))
        
        scrollView.addSubview(locationButton)
        locationButton.setConstraint(top: scrollView.topAnchor, leading: view.leadingAnchor, trailing: view.trailingAnchor, bottom: nil, padding: .init(top: 20, left: 20, bottom: 20, right: 20), size: .init(width: 0, height: 250))
        
        scrollView.addSubview(DeedNameTextField)
        
        DeedNameTextField.setConstraint(top: mapView.bottomAnchor, leading: view.leadingAnchor, trailing: view.trailingAnchor, bottom: nil, padding: .init(top: 20, left: 40, bottom: 0, right: 40), size: .init(width: 0, height: 0))
        
        textcontroller1 = MDCTextInputControllerOutlined(textInput: DeedNameTextField)
        // self.textcontroller.textInsets(UIEdgeInsets(top: 16, left: 16, bottom: 16, right: 16))
        textcontroller1.activeColor = colour.deepblue
        textcontroller1.normalColor = UIColor.gray
        textcontroller1.inlinePlaceholderFont =  UIFont(name: "AirbnbCerealApp-Book", size: 18)
        textcontroller1.inlinePlaceholderColor = UIColor.gray
        
        scrollView.addSubview(AddressTextField)
        AddressTextField.setConstraint(top: DeedNameTextField.bottomAnchor, leading: DeedNameTextField.leadingAnchor, trailing: DeedNameTextField.trailingAnchor, bottom: nil, padding: .init(top: 10, left: 0, bottom: 0, right: 0), size: .init(width: 0, height: 0))
        textcontroller2 = MDCTextInputControllerOutlinedTextArea(textInput: AddressTextField)
        textcontroller2.activeColor = colour.deepblue
        textcontroller2.normalColor = UIColor.gray
        textcontroller2.inlinePlaceholderFont =  UIFont(name: "AirbnbCerealApp-Book", size: 18)
        textcontroller2.inlinePlaceholderColor = UIColor.gray
        
            
        scrollView.addSubview(LandmarkTextField)
        LandmarkTextField.setConstraint(top: AddressTextField.bottomAnchor, leading: DeedNameTextField.leadingAnchor, trailing: DeedNameTextField.trailingAnchor, bottom: nil, padding: .init(top: 10, left: 0, bottom: 0, right: 0), size: .init(width: 0, height: 0))
       
                  textcontroller3 = MDCTextInputControllerOutlined(textInput: LandmarkTextField)
                  textcontroller3.activeColor = colour.deepblue
                  textcontroller3.normalColor = UIColor.gray
                  textcontroller3.inlinePlaceholderFont =  UIFont(name: "AirbnbCerealApp-Book", size: 18)
                  textcontroller3.inlinePlaceholderColor = UIColor.gray
        
        scrollView.addSubview(photoButton)
        photoButton.setConstraint(top: LandmarkTextField.bottomAnchor, leading: DeedNameTextField.leadingAnchor, trailing: view.trailingAnchor, bottom: nil, padding: .init(top: 10, left: 0, bottom: 0, right: 120), size: .init(width: 0, height: 50))
        
        scrollView.addSubview(PhotoCollectionView)
        PhotoCollectionView.setConstraint(top: photoButton.bottomAnchor, leading: photoButton.leadingAnchor, trailing: view.trailingAnchor, bottom: nil, padding: .init(top: 20, left: 0, bottom: 0, right: 10), size: .init(width: 0, height: 84))
        PhotoCollectionView.alpha = 0
        scrollView.addSubview(sumbitButton)
        sumbitButton.setConstraint(top: photoButton.bottomAnchor, leading: view.leadingAnchor, trailing: view.trailingAnchor, bottom: nil, padding: .init(top: 30, left: 70, bottom: 0, right: 70), size: .init(width: 0, height: 50))
        
        scrollView.addSubview(sumbitButton2)
        sumbitButton2.setConstraint(top: PhotoCollectionView.bottomAnchor, leading: view.leadingAnchor, trailing: view.trailingAnchor, bottom: nil, padding: .init(top: 30, left: 70, bottom: 0, right: 70), size: .init(width: 0, height: 50))
        sumbitButton2.alpha = 0
        
        
        
        
        
        
        let map = AGSMap(basemapType: .openStreetMap, latitude: 5.256685, longitude: 73.302311, levelOfDetail: 0)
        mapView.map = map
        mapView.alpha = 0.7
        locationButton.alpha = 0.4
       
        
        
//        scrollView.addSubview()
     
    }
    
    
    @objc func clickLocation(_ sender: Any) {
        
            let mapvc = storyboard?.instantiateViewController(identifier: "mapFormVIewController") as! mapFormVIewController
             mapvc.delegate = self
//            guard let latW = lat,let lonW = long else {return}
//            mapvc.latd = latW
//            mapvc.longd =  lonW
            print("in")
                self.navigationController?.pushViewController(mapvc, animated: true)
    }
    
    
    //UPLOAD PHOTO//
    @objc func upoadphoto(_ sender: Any) {
        MultiimagePickerController.modalPresentationStyle = .fullScreen
        present(self.MultiimagePickerController, animated: true, completion: nil)
    }
    



           
       
       
       
       
       func shouldDismissPhotoPicker(withTLPHAssets: [TLPHAsset]) -> Bool {
            PhotoCollectionView.alpha = 1
        
        if withTLPHAssets.count > 0 {
            sumbitButton.alpha = 0
            sumbitButton2.alpha = 1
        }else {
            sumbitButton.alpha = 1
            sumbitButton2.alpha = 0
        }
        
        
          
           mediaAssets.removeAll()
           selectedImage.removeAll()
           selectedAssets.removeAll()
           self.selectedAssets = withTLPHAssets
           
           for (index,value) in selectedAssets.enumerated() {
               //print(index)
               
               //ทำไมไม่ได้selectedImage[0] = selectedAssets[index].fullResolutionImage!
               selectedImage.append(value.fullResolutionImage!)
           }
           
           for (index,value) in selectedImage.enumerated() {
            mediaAssets.append(Media(image: value, key:"image",mime: appConfig.shared.con.allowed_image_formats[0])!)
           }
           
           PhotoCollectionView.reloadData()
           
           print(selectedImage)
           return true
       }
    
 
    
  //SUBMIT BUTTON//
    @objc func submitbutton(_ sender: UIButton) {
    sender.pulse()

       
        
        let check = checkTextField()
        if check == false {
            return
        }
        
     
        guard let latW = lat,let longW = long else{
                 return
             }
             
            
             let alert = UIAlertController(title: "Submit", message: "Do you want to request this?", preferredStyle: .alert)
             alert.addAction(UIAlertAction(title:"No",style: .cancel,handler: nil))
             alert.addAction(UIAlertAction(title:"Yes",style: .default,handler: {
                 (action:UIAlertAction) in
                 self.createLoader()
                 //self.loader.hidesWhenStopped = true
                 self.loader.center = self.view.center
                 self.view.addSubview(self.loader)
                 //self.loader.startAnimating()

               
                self.presenter.addingDetail(deed_name:self.DeedNameTextField.text!, address:self.AddressTextField.text! ,landmark:self.LandmarkTextField.text!,lat:latW,long:longW,images:self.mediaAssets)
                 
             }))
             
             present(alert,animated:true)
            
         }
    
    
    func createLoader(){
        
        view.addSubview(bgloader)
        bgloader.setConstraint(top: view.topAnchor, leading: view.leadingAnchor, trailing: view.trailingAnchor, bottom: view.bottomAnchor, padding: .init(top: 0, left: 0, bottom: 0, right: 0), size: .init(width: 0, height: 0))
        bgloader.addSubview(animatedView)
        animatedView.setConstraint(top: bgloader.topAnchor, leading: bgloader.leadingAnchor, trailing: bgloader.trailingAnchor, bottom: bgloader.bottomAnchor, padding: .init(top: 300, left: 50, bottom: 300, right: 50), size: .init(width: 0, height: 0))
        animatedView.play()
        
        bgloader.addSubview(loaderText)
        loaderText.setConstraint(top: animatedView.bottomAnchor, leading: nil, trailing: nil, bottom: nil)
        loaderText.centerXAnchor.constraint(equalTo: bgloader.centerXAnchor).isActive = true
        
    }
    
    func checkTextField() -> Bool{
        
         var check = true
             DeedNameTextField.resignFirstResponder()
             AddressTextField.resignFirstResponder()
             LandmarkTextField.resignFirstResponder()
         
         if DeedNameTextField.text == "" {
             textcontroller1.helperText = "Please fill the information"
             textcontroller1.leadingUnderlineLabelTextColor = UIColor.red
             textcontroller1.normalColor = UIColor.red
             
             check = false
         }
         else {
             textcontroller1.helperText = ""
             textcontroller1.normalColor = UIColor.gray
         }
         if AddressTextField.text == "" {
             textcontroller2.helperText = "Please fill the information"
             textcontroller2.leadingUnderlineLabelTextColor = UIColor.red
             textcontroller2.normalColor = UIColor.red
            
             check = false
         }
         else {
             textcontroller2.helperText = ""
             textcontroller2.normalColor = UIColor.gray
         }
        
        if lat == nil || long == nil {
            mapView.layer.borderColor = UIColor.red.cgColor
            mapView.layer.borderWidth = 5
        }
        else {
            mapView.layer.borderWidth = 0
        }
         return check
     }
    

    
    
    
 
       
       
    

}


extension Form2ViewController:sendDataDelegate,apiFinishDelegate{
    func apiFinish() {
       // self.loader.stopAnimating()
        bgloader.removeFromSuperview()
        let listvc = self.storyboard?.instantiateViewController(withIdentifier: "TabbarController") as! UITabBarController
        self.navigationController?.pushViewController(listvc, animated: false)
    }
    
    func dropPinSuccessfully(lat: String, long: String,text:String = "") {
        locationButton.alpha = 0
        self.lat = lat
        self.long = long
        let latd = (self.lat as NSString).doubleValue
        let longd = (self.long as NSString).doubleValue
        mapView.layer.borderWidth = 0 
        AddressTextField.text = text
        
        mapView.alpha = 1
//       mapView.setViewpointCenter(AGSPoint(x: longd, y: latd, spatialReference: .wgs84()))
        mapView.setViewpointCenter(AGSPoint(x: longd, y: latd, spatialReference: .wgs84()), scale: 10)
        createGraphics(lat:latd,lon:longd)
        
        
    }
    
    func geoView(_ geoView: AGSGeoView, didTapAtScreenPoint screenPoint: CGPoint, mapPoint: AGSPoint) {

               let mapvc = storyboard?.instantiateViewController(identifier: "mapFormVIewController") as! mapFormVIewController
                   mapvc.delegate = self
               if let latw = lat {
               mapvc.latd = latw
           }
                if let longw = long {
               mapvc.longd = longw
           }
           self.navigationController?.pushViewController(mapvc, animated: true)

       }
       
       
    
    private func createGraphics(lat:Double,lon:Double) {
              
          
            graphicsOverlay.graphics.removeAllObjects()
              self.mapView.graphicsOverlays.add(graphicsOverlay)
              let markerImage = UIImage(named: "MAP-LOGO")!
              let symbol = AGSPictureMarkerSymbol(image: markerImage)
              //symbol.offsetX = markerImage.size.height
              symbol.offsetY = markerImage.size.height/2
             // symbol.leaderOffsetY = markerImage.size.height / 2
             // symbol.offsetX = markerImage.size.height/2
             // let pinSymbol = AGSSimpleMarkerSymbol(style: .circle, color: .blue, size: 5)
              let point = AGSPoint(x:lon, y: lat, spatialReference: AGSSpatialReference.wgs84())
                  
              
              let imgGraphic = AGSGraphic(geometry: point, symbol: symbol, attributes: nil)
            
              graphicsOverlay.graphics.add(imgGraphic)
              }
}


extension Form2ViewController:UICollectionViewDataSource,UICollectionViewDelegate{
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return selectedImage.count
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = PhotoCollectionView.dequeueReusableCell(withReuseIdentifier: "PhotoCollectionViewCell", for: indexPath) as! PhotoCollectionViewCell
        cell.setup(image: selectedImage[indexPath.row])
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.present(self.MultiimagePickerController, animated: true, completion: nil)
        
    }
    
    
}



