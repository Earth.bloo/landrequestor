//
//  File.swift
//  DeedRequestApplication
//
//  Created by admin on 14/2/2563 BE.
//  Copyright © 2563 EarthBlooProduction. All rights reserved.
//

import Foundation
import ArcGIS
import Alamofire

protocol apiFinishDelegate{
    func apiFinish()
    
}

class FormPresenter:NSObject  {
    // var deed:AllDeed!
    
    typealias Parameter = [String:String]
    var param: [String:String]!
    var delegate:apiFinishDelegate!
   
    var selectedImages2 : Media!
    var mediaAsset2:[Media]!
    var url:NSURL!
    var imageData: NSData!
    var user_id = UserDefaults.standard.integer(forKey: "User_id")
    
    private let session: Session
    
    override init() {
        session = Alamofire.Session.default
        session.sessionConfiguration.timeoutIntervalForRequest = TimeInterval(1500)
        session.sessionConfiguration.httpCookieAcceptPolicy = HTTPCookie.AcceptPolicy.always
    }
    
    
    func addingDetail(deed_name:String,address:String,landmark:String,lat:String,long:String,images:[Media]) {
        
        mediaAsset2 = images  
        param =
               ["customer_id":"\(user_id)",
                "deed_name":"\(deed_name)",
                "address":"\(address)",
                "nearby_landmark":"\(landmark)",
                "latitude":"\(lat)",
                "longitude":"\(long)"]
        
        print("1")
        alamofireAPI()
    }
    

    
    
    func alamofireAPI() {
       
        // https://us-east-1-api-landsurveyor.herokuapp.com/api/v1/deeds
        let url: String = "http://land-surveyor-dev.eba-atp2ae9g.ap-southeast-1.elasticbeanstalk.com/api/v1/deeds"
        session.upload(multipartFormData: { (multipartFormData) in
            
            // image
            for (index, value) in self.mediaAsset2.enumerated() {
                multipartFormData.append(self.mediaAsset2[index].data, withName: "image", fileName: self.mediaAsset2[index].filename, mimeType: self.mediaAsset2[index].mimeType)
                
            }
            // data
            for (key, value) in self.param {
                guard let data = value.data(using: .utf8) else { continue }
                multipartFormData.append(data, withName: key)
            }
        }, to: url as URLConvertible).uploadProgress(queue: .main, closure: { progress in
            print(progress)
            }).responseJSON(completionHandler: { data in
                print(data)
                self.delegate.apiFinish()
                print(3)
                
            })
        
        
    }
    
    
    
    
   
    
}










