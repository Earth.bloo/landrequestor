//
//  Media.swift
//  DeedRequestApplication
//
//  Created by admin on 1/3/2563 BE.
//  Copyright © 2563 EarthBlooProduction. All rights reserved.
//

import UIKit

struct Media {
    let key:String!
    let filename:String!
    let data:Data!
    let mimeType:String!
    
    init?(image:UIImage,key:String,mime:String) {
        self.key = key
        self.mimeType = mime
        self.filename = "\(arc4random()).jpeg"
        guard let data = image.jpegData(compressionQuality: 0.1) else {return nil}
        self.data = data
    }
}
