//
//  ViewController.swift
//  DeedRequestApplication
//
//  Created by admin on 14/2/2563 BE.
//  Copyright © 2563 EarthBlooProduction. All rights reserved.
//

import UIKit
import ArcGIS
import TLPhotoPicker
import MaterialComponents.MaterialTextFields
import SkeletonView

class ViewController: UIViewController,AGSGeoViewTouchDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,TLPhotosPickerViewControllerDelegate{
    
    
    @IBOutlet weak var locationButton: UIButton!
    var count = 0
    
    let loader:UIActivityIndicatorView = {
        let loader = UIActivityIndicatorView()
        loader.style = .large
        return loader
    }()
    
    var DeedNameTextField:MDCTextField = {
        let view = MDCTextField()
        view.placeholder = "DEED NAME"
        view.font = UIFont(name: "AirbnbCerealApp-Book", size: 18)      
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    var textcontroller1:MDCTextInputControllerOutlined!
    
    var AddressTextField: MDCTextField = {
        let view = MDCTextField()
        view.placeholder = "ADDRESS"
        view.font = UIFont(name: "AirbnbCerealApp-Book", size: 16)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    var textcontroller2:MDCTextInputControllerOutlined!
    
    var LandMarkTextField:MDCTextField = {
        let view = MDCTextField()
        view.placeholder = "LANDMARK"
        view.font = UIFont(name: "AirbnbCerealApp-Book", size: 18)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    var textcontroller3:MDCTextInputControllerOutlined!
    
    
    @IBOutlet weak var PhotoCollectionView: UICollectionView!
    @IBOutlet weak var submitButton: UIButton!
    
    
    let mapView:AGSMapView = {
        let view = AGSMapView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    private var graphicsOverlay:AGSGraphicsOverlay!
    
    
    //upload latitude and longitude
    var lat:String!
    var long:String!
    
    //upload photo
    var selectedImage:[UIImage] = []
    var selectedAssets = [TLPHAsset]()
    var mediaAssets:[Media] = []
    var MultiimagePickerController:TLPhotosPickerViewController!
    
    
    //map
    var point = AGSPoint(x: 0, y: 0, spatialReference: .webMercator())
    var mapForm:mapFormVIewController!
   // private var map: AGSMap!
     //private let graphicsOverlay = AGSGraphicsOverlay()
    
    
    
    //presenter
    var presenter:FormPresenter!
    
    
    
    
    
    
    
    
    override func viewDidLoad(){
        super.viewDidLoad()
        mapForm = mapFormVIewController()
        view.isSkeletonable = true
        
        
//        DeedNameTextField.becomeFirstResponder()
//        AddressTextField.becomeFirstResponder()
//        LandMarkTextField.becomeFirstResponder()
        
        presenter = FormPresenter()
        mapView.touchDelegate = self
        presenter.delegate = self
        MultiimagePickerController = TLPhotosPickerViewController()
        MultiimagePickerController.delegate = self
        PhotoCollectionView.delegate = self
        PhotoCollectionView.dataSource = self
        
        PhotoCollectionView.register(UINib(nibName: "PhotoCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "PhotoCollectionViewCell")
        graphicsOverlay = AGSGraphicsOverlay()
        setUI()
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        locationButton.layer.borderWidth = 0
        textcontroller1.normalColor = UIColor.gray
        textcontroller2.normalColor = UIColor.gray
        textcontroller3.normalColor = UIColor.gray
        textcontroller1.inlinePlaceholderColor = UIColor.gray
        textcontroller2.inlinePlaceholderColor = UIColor.gray
        textcontroller3.inlinePlaceholderColor = UIColor.gray
    }
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        
        //navigation Bar
        let titlelabel = UILabel(frame: CGRect(x: 0, y: 0, width: view.frame.width , height: view.frame.height))
        titlelabel.text  = "Add Request"
        
        titlelabel.font = UIFont(name:"AirbnbCerealApp-Bold",size: 23)
        titlelabel.textColor = UIColor.white
        self.tabBarController?.navigationItem.titleView = titlelabel
    }
    
    func setUI(){
        submitButton.layer.cornerRadius = 10.0
        
        
        view.addSubview(DeedNameTextField)
        DeedNameTextField.topAnchor.constraint(equalTo: locationButton.bottomAnchor, constant: 20).isActive = true
        DeedNameTextField.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        DeedNameTextField.widthAnchor.constraint(equalTo:locationButton.widthAnchor).isActive = true
        // firstnameTextField.heightAnchor.constraint(equalToConstant: 30).isActive = true
       textcontroller1 = MDCTextInputControllerOutlined(textInput: DeedNameTextField)
        // self.textcontroller.textInsets(UIEdgeInsets(top: 16, left: 16, bottom: 16, right: 16))
        textcontroller1.activeColor = colour.deepblue
        textcontroller1.normalColor = UIColor.gray
        textcontroller1.inlinePlaceholderFont =  UIFont(name: "AirbnbCerealApp-Book", size: 17)
        textcontroller1.inlinePlaceholderColor = UIColor.gray
        
        
        
        view.addSubview(AddressTextField)
        AddressTextField.topAnchor.constraint(equalTo: DeedNameTextField.bottomAnchor, constant: -10).isActive = true
        AddressTextField.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        AddressTextField.widthAnchor.constraint(equalTo:locationButton.widthAnchor).isActive = true
         //AddressTextField.heightAnchor.constraint(equalToConstant: 100).isActive = true
       textcontroller2 = MDCTextInputControllerOutlined(textInput: AddressTextField)
       // self.textcontroller.textInsets(UIEdgeInsets(top: 16, left: 16, bottom: 16, right: 16))
       textcontroller2.activeColor = colour.deepblue
       textcontroller2.normalColor = UIColor.gray
       textcontroller2.inlinePlaceholderFont =  UIFont(name: "AirbnbCerealApp-Book", size: 17)
        textcontroller2.inlinePlaceholderColor = UIColor.gray
        
        view.addSubview(LandMarkTextField)
        LandMarkTextField.topAnchor.constraint(equalTo: AddressTextField.bottomAnchor, constant: -10).isActive = true
        LandMarkTextField.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        LandMarkTextField.widthAnchor.constraint(equalTo:locationButton.widthAnchor).isActive = true
        // PNTextField.heightAnchor.constraint(equalToConstant: 30).isActive = true
        textcontroller3 = MDCTextInputControllerOutlined(textInput: LandMarkTextField)
        //  self.textcontroller.textInsets(UIEdgeInsets(top: 16, left: 16, bottom: 16, right: 16))
       textcontroller3.activeColor = colour.deepblue
       textcontroller3.normalColor = UIColor.gray
       textcontroller3.inlinePlaceholderFont =  UIFont(name: "AirbnbCerealApp-Book", size: 17)
        textcontroller3.inlinePlaceholderColor = UIColor.gray
        view.addSubview(mapView)
        mapView.translatesAutoresizingMaskIntoConstraints = false
        mapView.leadingAnchor.constraint(equalTo:locationButton.leadingAnchor).isActive = true
        mapView.trailingAnchor.constraint(equalTo:locationButton.trailingAnchor).isActive = true
        mapView.bottomAnchor.constraint(equalTo:locationButton.bottomAnchor).isActive = true
        mapView.topAnchor.constraint(equalTo:locationButton.topAnchor).isActive = true
        let map = AGSMap(basemapType: .openStreetMap, latitude: 5.256685, longitude: 73.302311, levelOfDetail: 0)
        mapView.map = map
        mapView.alpha = 0.7
        locationButton.alpha = 0.7
        // self.textcontroller3.inlinePlaceholderColor = colour.deepblue
    }
    
    
    
    
    @IBAction func submitbutton(_ sender: UIButton) {
        sender.pulse()
        
        if DeedNameTextField.text! == "" {
            self.textcontroller1.normalColor = UIColor.red
            self.textcontroller1.inlinePlaceholderColor = UIColor.red
            return
        }
        else {
            self.textcontroller1.normalColor = UIColor.black
            self.textcontroller1.inlinePlaceholderColor = UIColor.black
        }
        if AddressTextField.text! == "" {
            self.textcontroller2.normalColor = UIColor.red
            self.textcontroller2.inlinePlaceholderColor = UIColor.red
            
        }else {
            self.textcontroller2.normalColor = UIColor.black
            self.textcontroller2.inlinePlaceholderColor = UIColor.black
        }
        
        guard let lat = lat,let long = long  else{
            locationButton.layer.borderWidth = 5
            locationButton.layer.borderColor = UIColor.red.cgColor
            return
        }
        
        
        
        let alert = UIAlertController(title: "Submit", message: "Do you want to request this?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title:"No",style: .cancel,handler: nil))
        alert.addAction(UIAlertAction(title:"Yes",style: .default,handler: {
            (action:UIAlertAction) in
            
            self.loader.hidesWhenStopped = true
            self.loader.center = self.view.center
            self.view.addSubview(self.loader)
            self.loader.startAnimating()
            
            print("sent = \(lat) and \(long)")
            
            self.presenter.addingDetail(deed_name:self.DeedNameTextField.text!, address:self.AddressTextField.text! ,landmark:self.LandMarkTextField.text!,lat:lat,long:long,images:self.mediaAssets)
            
        }))
        
        present(alert,animated:true)
        
    }
    
    @IBAction func clickLocation(_ sender: Any) {
        
                let mapvc = storyboard?.instantiateViewController(identifier: "mapFormVIewController") as! mapFormVIewController
                    mapvc.delegate = self
            guard let latW = lat,let lonW = long else {return}
            mapvc.latd = latW
            mapvc.longd =  lonW
                self.navigationController?.pushViewController(mapvc, animated: true)
    }
    
    
    //----------------UPLOAD PHOTO----------------//
    
    @IBAction func upoadphoto(_ sender: Any) {
        present(self.MultiimagePickerController, animated: true, completion: nil)
    }

        
    
    
    
    
    func shouldDismissPhotoPicker(withTLPHAssets: [TLPHAsset]) -> Bool {
      let photoAlert = UIAlertController(title: "Error", message: "You can select only 4 photos", preferredStyle: .alert)
      photoAlert.addAction(UIAlertAction(title:"Ok",style: .cancel,handler: nil))
        
       
        mediaAssets.removeAll()
        selectedImage.removeAll()
        selectedAssets.removeAll()
        self.selectedAssets = withTLPHAssets
        
        for (index,value) in selectedAssets.enumerated() {
            //print(index)
            
            //ทำไมไม่ได้selectedImage[0] = selectedAssets[index].fullResolutionImage!
            selectedImage.append(value.fullResolutionImage!)
        }
        
        for (index,value) in selectedImage.enumerated() {
            mediaAssets.append(Media(image: value, key:"image")!)
        }
        
        PhotoCollectionView.reloadData()
        
        print(selectedImage)
        return true
    }
    
    
  
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    //    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
    //
    //        labelUploadAlready.text = "Upload Photo Already"
    //        labelUploadAlready.textColor = UIColor.green
    //        let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
    //        dismiss(animated: true,completion: nil)
    //
    //        guard let img = image else { return }
    //       // selectedImage = img
    //
    //
    //
    //
    //
    //    }
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if segue.identifier == "mapFormVIewController" {
//
//            mapForm = segue.destination as? mapFormVIewController
//            mapForm.delegate = self
//            guard let latW = lat,let lonW = long else {return}
//            mapForm.latd = latW
//            mapForm.longd = lonW
//        }
//
//
//    }
    
}

extension ViewController:sendDataDelegate,apiFinishDelegate{
    func apiFinish() {
        self.loader.stopAnimating()
        
        let listvc = self.storyboard?.instantiateViewController(withIdentifier: "TabbarController") as! UITabBarController
        self.navigationController?.pushViewController(listvc, animated: false)
    }
    
    func dropPinSuccessfully(lat: String, long: String,text:String = "") {
        locationButton.alpha = 0 
        self.lat = lat
        self.long = long
        let latd = (self.lat as NSString).doubleValue
        let longd = (self.long as NSString).doubleValue
        
        AddressTextField.text = text
        AddressTextField.resignFirstResponder()
        mapView.alpha = 1
//        mapView.setViewpointCenter(AGSPoint(x: longd, y: latd, spatialReference: .wgs84()))
        mapView.setViewpointCenter(AGSPoint(x: longd, y: latd, spatialReference: .wgs84()), scale: 10)
        createGraphics(lat:latd,lon:longd)
        
        
    }
    
     private func createGraphics(lat:Double,lon:Double) {
           
       
         graphicsOverlay.graphics.removeAllObjects()
           self.mapView.graphicsOverlays.add(graphicsOverlay)
           let markerImage = UIImage(named: "MAP-LOGO")!
           let symbol = AGSPictureMarkerSymbol(image: markerImage)
           //symbol.offsetX = markerImage.size.height
           symbol.offsetY = markerImage.size.height/2
          // symbol.leaderOffsetY = markerImage.size.height / 2
          // symbol.offsetX = markerImage.size.height/2
          // let pinSymbol = AGSSimpleMarkerSymbol(style: .circle, color: .blue, size: 5)
           let point = AGSPoint(x:lon, y: lat, spatialReference: AGSSpatialReference.wgs84())
               
           
           let imgGraphic = AGSGraphic(geometry: point, symbol: symbol, attributes: nil)
         
           graphicsOverlay.graphics.add(imgGraphic)
           }
    
    func geoView(_ geoView: AGSGeoView, didTapAtScreenPoint screenPoint: CGPoint, mapPoint: AGSPoint) {

            let mapvc = storyboard?.instantiateViewController(identifier: "mapFormVIewController") as! mapFormVIewController
                mapvc.delegate = self
            if let latw = lat {
            mapvc.latd = latw
        }
             if let longw = long {
            mapvc.longd = longw
        }
        self.navigationController?.pushViewController(mapvc, animated: true)

    }
    
    
    
    
}

//----------------COOLECTION VIEW----------------//

extension ViewController:UICollectionViewDataSource,UICollectionViewDelegate{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return selectedImage.count 
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = PhotoCollectionView.dequeueReusableCell(withReuseIdentifier: "PhotoCollectionViewCell", for: indexPath) as! PhotoCollectionViewCell
        cell.setup(image: selectedImage[indexPath.row])
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.present(self.MultiimagePickerController, animated: true, completion: nil)
        
    }
    
    
}












