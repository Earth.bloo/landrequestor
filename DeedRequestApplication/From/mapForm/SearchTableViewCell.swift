//
//  SearchTableViewCell.swift
//  DeedRequestApplication
//
//  Created by admin on 19/3/2563 BE.
//  Copyright © 2563 EarthBlooProduction. All rights reserved.
//

import UIKit

class SearchTableViewCell: UITableViewCell {
    @IBOutlet weak var searchLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
      
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setlabel(labels:String){
        self.searchLabel.text = labels
    }
    
}
