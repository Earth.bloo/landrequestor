//
//  mapFormPresenter.swift
//  DeedRequestApplication
//
//  Created by admin on 19/3/2563 BE.
//  Copyright © 2563 EarthBlooProduction. All rights reserved.
//

import Foundation

protocol selectMapDelegate{
    func setData(suggest:suggestionSearch)
    func sendLatLong(lat:Double,long:Double)
    
}



class mapFormPreseter{
    var delegate:selectMapDelegate!
    
    
    //API Variable
    var suggest:suggestionSearch?
    var selectedDetail:selectSearch?
    
    
    
    
    
    
    
    func searchAPI(searchbartext:String){
        var semaphore = DispatchSemaphore (value: 0)

        let parameters = "{\n  \"keyword\": \"\(searchbartext)\",\n  \"key\": \"9b8d91ead459b737e3265025c3bfde1c79b76a9a80e3e9af7060165e94ed7c8892f4fa24916483b2\",\n  \"maxResult\": 5\n}"
        let postData = parameters.data(using: .utf8)

        var request = URLRequest(url: URL(string: "https://geosearch.cdg.co.th/g/search/autocomplete")!,timeoutInterval: Double.infinity)
        request.addValue("atlasx.cdg.co.th", forHTTPHeaderField: "referer")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")

        request.httpMethod = "POST"
        request.httpBody = postData

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
          guard let data = data else {
            
            return
          }
           do{
                let decoder = JSONDecoder()
                self.suggest =  try decoder.decode(suggestionSearch.self, from: data)
           // print(self.suggest?.data[0].LocationID)
           }catch{
                            print(String(describing: error))
                        }

          print(String(data: data, encoding: .utf8)!)
          semaphore.signal()
        }

        task.resume()
        semaphore.wait()
        guard let suggestw = suggest else {
            return
        }
        delegate.setData(suggest: suggestw)
    }
    
    
    func GetDetailAPI(keydetail:String){
        var semaphore = DispatchSemaphore (value: 0)

        let parameters = "{\n    \"locationid\": \"\(keydetail)\",\n    \"key\": \"9b8d91ead459b737e3265025c3bfde1c79b76a9a80e3e9af7060165e94ed7c8892f4fa24916483b2\"\n  }"
        let postData = parameters.data(using: .utf8)

        var request = URLRequest(url: URL(string: "https://geosearch.cdg.co.th/g/search/details")!,timeoutInterval: Double.infinity)
        request.addValue("atlasx.cdg.co.th", forHTTPHeaderField: "referer")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")

        request.httpMethod = "POST"
        request.httpBody = postData

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
          guard let data = data else {
            print(String(describing: error))
            return
          }
            do{
                           let decoder = JSONDecoder()
                           self.selectedDetail =  try decoder.decode(selectSearch.self, from: data)
                
                      }catch{
                            print(String(describing: error))
                        
                                }
            
          
          semaphore.signal()
        }

        task.resume()
        semaphore.wait()
        
        guard let lat_lon = self.selectedDetail?.data?.LAT_LON else {
            print("cant get latitude longitude")
            return
            
        }
        let fullNameArr = lat_lon.components(separatedBy: ",")
        var lat: String = fullNameArr[0]
        var long: String = fullNameArr[1]
        long = String(long.dropFirst())
        let latd = Double(lat)
        let longd = Double(long)
        guard let latUW = latd,let longUW = longd else{
            print("problem with type lat and long ")
            return
        }
        
        delegate.sendLatLong(lat:latUW,long:longUW)
        
        
    }
    
    
    
    
    
    
    
    
}
