//
//  mapFormVIewController.swift
//  DeedRequestApplication
//
//  Created by admin on 5/3/2563 BE.
//  Copyright © 2563 EarthBlooProduction. All rights reserved.
//

import UIKit
import ArcGIS

protocol sendDataDelegate:AnyObject{
    func dropPinSuccessfully(lat:String,long:String,text:String)
    
}
class mapFormVIewController: UIViewController,AGSGeoViewTouchDelegate {
  
   weak var delegate:sendDataDelegate?
   
    var searchTemp:String!
    var suggest:suggestionSearch!
    var CGpo:CGPoint!
    
  
  
    
    let pinImage:UIImageView = {
        let view = UIImageView()
        view.image = UIImage(named: "MAP-LOGO")
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let curlocatButton:UIButton = {
        let view = UIButton()
        //view.backgroundColor = UIColor.white
        let cursor = UIImage(named: "current_btn")
        view.setBackgroundImage(cursor, for: .normal)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.addTarget(self, action: #selector(setupLocationDisplay), for: .touchUpInside)
        
        return view
    }()
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    var latd:String = "13.74444"
    var longd:String = "100.56666"
    var presenter:mapFormPreseter!

    @IBOutlet weak var pinView: UIImageView!
    @IBOutlet weak var imagePin: UIImageView!
    @IBOutlet weak var mapView: AGSMapView!
    private let graphicsOverlay = AGSGraphicsOverlay()
    private var map: AGSMap!
    var point = AGSPoint(x: 0, y: 0, spatialReference: .webMercator())
    let phonepoint =  CGPoint(x:208.0,y:402.0)
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUI()
        presenter = mapFormPreseter()
        presenter.delegate = self
        mapView.touchDelegate = self
       // setupLocationDisplay()
        let x = (self.latd as NSString).doubleValue
        let y = (self.longd as NSString).doubleValue
        setupMap(lat:x,long: y)
        setupPin()
        
      
        
        //search Part
         tableView.alpha = -200
         self.searchBar.delegate = self
         let nibdf = UINib(nibName: "SearchTableViewCell", bundle: nil)
         tableView.register(nibdf, forCellReuseIdentifier: "SearchTableViewCell")
         tableView.tableHeaderView?.removeFromSuperview()
         tableView.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: CGFloat.leastNonzeroMagnitude))
         tableView.layoutIfNeeded()

        // Do any additional setup after loading the view.
        
        
    }
    
    func setUI(){
        

        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(barButtonSystemItem: UIBarButtonItem.SystemItem.done, target: self, action: #selector(verifylocation))
               self.navigationItem.rightBarButtonItem?.tintColor = UIColor.white
                
              
               
        self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(barButtonSystemItem: UIBarButtonItem.SystemItem.cancel, target: self, action: #selector(cancel))
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        
        mapView.addSubview(curlocatButton)
        let CurButtonConstraint = [
            curlocatButton.bottomAnchor.constraint(equalTo: mapView.bottomAnchor, constant: -50),
            curlocatButton.leadingAnchor.constraint(equalTo: mapView.leadingAnchor, constant: 15),
            curlocatButton.widthAnchor.constraint(equalToConstant: 40),
            curlocatButton.heightAnchor.constraint(equalToConstant: 40)
        ]
        NSLayoutConstraint.activate(CurButtonConstraint)
        
     
        
    }
    
    func setupMap(lat:Double,long:Double){
        
         // imagePin.image  = UIImage(named: "MAP-LOGO")
        map = AGSMap(basemapType: .openStreetMap, latitude: lat, longitude: long, levelOfDetail: 20)

                  mapView.map = map
                  //add graphics overlay to the map view
                  mapView.graphicsOverlays.add(graphicsOverlay)
                   
                  //touch delegate for map view
                  mapView.touchDelegate = self
                  
       }
    
    @objc func setupLocationDisplay() {
        mapView.locationDisplay.autoPanMode = AGSLocationDisplayAutoPanMode.recenter
        mapView.locationDisplay.start { [weak self] (error:Error?) -> Void in
            if let error = error {
                self?.showAlert(withStatus: error.localizedDescription)
            }
           // self?.mapView.locationDisplay.stop()
            
        }
        
    }
    private func showAlert(withStatus: String) {
        let alertController = UIAlertController(title: "Alert", message:
            withStatus, preferredStyle: UIAlertController.Style.alert)
        alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertAction.Style.default,handler: nil))
        present(alertController, animated: true, completion: nil)
    }
    
    
     func goMapSearch(lat:Double,long:Double){
        
      mapView.locationDisplay.stop()
//      map = AGSMap(basemapType: .openStreetMap, latitude: lat, longitude: long, levelOfDetail: 20)
//
//      mapView.map = map
        
        mapView.viewpointChangedHandler = nil
        
        mapView.setViewpointCenter(AGSPoint(x: long, y: lat, spatialReference: .wgs84()), completion: { success in
                   self.checkPoint()
               })
      
      
               
    }

       
       
       
       
       private func displayGraphicAtPoint(_ point: AGSPoint) {
              //remove previous graphic from graphics overlay
              graphicsOverlay.graphics.removeAllObjects()
              
              //add graphic at tapped location
              let pinSymbol = AGSPictureMarkerSymbol(image: UIImage(named: "MAP-LOGO")!)
              let graphic = AGSGraphic(geometry: point, symbol: pinSymbol, attributes: nil)
              graphicsOverlay.graphics.add(graphic)
          }
    
   
  
    @objc func verifylocation() {
        guard let geometry = mapView.currentViewpoint(with: .centerAndScale)?.targetGeometry else { return }
        let extent = geometry.extent
       let cen = extent.center
        
        
//        let y = CGpo!
//        let x = mapView.screen(toLocation: y)
        let myMarkerSymbol = AGSSimpleMarkerSymbol(style: .circle, color: .green, size: 4)
        let myMarkerPoint = cen
        let myGraphic = AGSGraphic(geometry: myMarkerPoint, symbol: myMarkerSymbol, attributes: nil)
            graphicsOverlay.graphics.add(myGraphic)
        

        let a = AGSCoordinateFormatter.latitudeLongitudeString(from: cen, format: .decimalDegrees , decimalPlaces: 12)
        guard let latlong = a else {
            print("cant get lat long")
            return
        }
        
        
        let fullNameArr = latlong.components(separatedBy: " ")

        let lat: String = fullNameArr[0]
        latd = String(lat.dropLast())
        let long: String = fullNameArr[1]
        longd = String(long.dropLast())
        print("drop before show \(lat) and \(long)")
        let searchString = searchBar.text
        delegate?.dropPinSuccessfully(lat: latd, long: longd,text: searchString!)
        self.navigationController?.popViewController(animated: true)
   
    }
    
    @objc func cancel(){
         self.navigationController?.popViewController(animated: true)
    }
    
    
//     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//           guard let listViewController = segue.destination as? ViewController else {
//               return
//           }
//           listViewController.lat  = latd
//           listViewController.long  = longd
//           print("call prepare")
//
//
//       }
    
       
        
  
    func setupPin(){
//        let firstDotView = UIView(frame: CGRect(x: 0,
//                                                y: 0 ,
//                                                width: 10,
//                                                height: 10))
//        CGpo = CGPoint(x: mapView.bounds.width  / 2,
//        y: mapView.bounds.height / 2 )
//        firstDotView.center = CGpo
//        firstDotView.layer.cornerRadius  = 20/2
//        firstDotView.backgroundColor = UIColor.yellow
//        firstDotView.translatesAutoresizingMaskIntoConstraints = false
//        mapView.addSubview(firstDotView)
        
        view.addSubview(pinImage)
        pinImage.centerXAnchor.constraint(equalTo:mapView.centerXAnchor).isActive = true
        pinImage.widthAnchor.constraint(equalToConstant: 50).isActive = true
        pinImage.heightAnchor.constraint(equalToConstant: 50).isActive = true
        pinImage.centerYAnchor.constraint(lessThanOrEqualTo: mapView.centerYAnchor, constant: -37).isActive = true
        
    }
    

//    func geoView(_ geoView: AGSGeoView, didTouchDownAtScreenPoint screenPoint: CGPoint, mapPoint: AGSPoint, completion: @escaping (Bool) -> Void) {
//         completion(true)
//    }
//
    func checkPoint(){
        mapView.viewpointChangedHandler = {
            DispatchQueue.main.async {
                self.searchBar.text = ""
                
            }
            
        }
        
//        mapView.viewpointChangedHandler = nil
    }
    
    
}
    
    

    
    
    
    
    
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */



extension mapFormVIewController:UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return suggest.data.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchTableViewCell",for:indexPath) as! SearchTableViewCell
        
         cell.setlabel(labels: (suggest?.data[indexPath.row].FormattedAddress)!)
          return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
              return 50
          }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        pinImage.alpha = 100
        
        searchTemp = suggest?.data[indexPath.row].FormattedAddress

        guard let searchwrap = searchTemp else{return}
        searchBar.text = searchwrap
        
        let detail_ID = suggest?.data[indexPath.row].LocationID
        
        guard let detail = detail_ID else {return}
        presenter.GetDetailAPI(keydetail: detail)
        
    }
    
    
    
    
}



extension mapFormVIewController:UISearchBarDelegate{
   
        
        func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
            tableView.alpha = -100
            pinImage.alpha = 100
        }
        
        func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
            tableView.alpha = 100
            pinImage.alpha = -100
            
            tableView.reloadData()
            self.tableView.delegate = self
            self.tableView.dataSource = self
            guard let text = searchBar.text else { return }
            presenter.searchAPI(searchbartext: text)
           // print(searchBar.text) 
            tableView.reloadData()
        
            
            
        }
    
    
        func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
             //checkPoint()
             searchBar.showsCancelButton = true
             tableView.alpha = 100
             pinImage.alpha = -100
            
             searchBar.becomeFirstResponder()
        }
        func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
            searchBar.showsCancelButton = false
            tableView.alpha = -100
            pinImage.alpha = 100
            searchBar.resignFirstResponder()
            
            UIView.animate(withDuration: 0.3, animations: {
                self.view.layoutIfNeeded()
            })
        }
        
    
}

extension mapFormVIewController:selectMapDelegate{
    
    
    func setData(suggest: suggestionSearch) {
        self.suggest = suggest
        
    }
    
    func sendLatLong(lat:Double,long:Double){
        tableView.alpha = -100
        
        
        
        
         
       goMapSearch(lat: lat, long: long)
    }
    
    
}
