//
//  DetailSearch.swift
//  DeedRequestApplication
//
//  Created by admin on 20/3/2563 BE.
//  Copyright © 2563 EarthBlooProduction. All rights reserved.
//

import Foundation

struct selectSearch:Decodable {
    var data:detailofDeed?
    
}

struct detailofDeed:Decodable {
    var LocationID:String!
    var BusinessName:String?
    var HouseNumber:String?
    var PremiseName:String?
    var PremiseLaneName:String?
    var Moo:String?
    var StreetLeadingType:String?
    var StreetName:String?
    var StreetTrailingType:String?
    var SubStreetLeadingType:String?
    var SubStreetName:String?
    var SubStreetTrailingType:String?
    var StreetFullName:String?
    var SubDistrictPrefix:String?
    var SubDistrict:String?
    var DistrictPrefix:String?
    var District:String?
    var ProvincePrefix:String?
    var Province:String?
    var LanguageCode:String?
    var LAT_LON:String!
    var PostalCode:String?
    var FormattedAddress:String?
    
    
    
}
