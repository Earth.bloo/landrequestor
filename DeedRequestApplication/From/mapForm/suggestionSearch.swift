//
//  SuggestionSearch.swift
//  DeedRequestApplication
//
//  Created by admin on 19/3/2563 BE.
//  Copyright © 2563 EarthBlooProduction. All rights reserved.
//

import Foundation

struct suggestionSearch:Decodable {
    var data:[detail]!
}

struct detail:Decodable{
    var LocationID:String!
    var FormattedAddress:String!
}
