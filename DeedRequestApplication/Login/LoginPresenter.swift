//
//  LoginPresenter.swift
//  DeedRequestApplication
//
//  Created by admin on 17/2/2563 BE.
//  Copyright © 2563 EarthBlooProduction. All rights reserved.
//
//struct Person:Codable{
//    var user_id: Int
//}


import UIKit
import Foundation

protocol VCDelegate{
    func showMessage(message:String)
    func UserSuccessfullyLogin(id:Int)
    func UserUnsuccessfullyLogin()
}

class LoginPresenter: NSObject {
    var delegate:VCDelegate!
    
    func test(text: String) -> Int {
        return 0
    }
   
    func validateLogin(id:String,password:String){
        if id.count != 0 {
            if password.count != 0 {
                verifyIDandPasswithAPI(id: id, password: password)
               
                
            }else {
            delegate.showMessage(message: "please password id properly")
            }
        

        }else { 
            
                delegate.showMessage(message: "please insert id properly")
            }
        }
    
   
    func verifyIDandPasswithAPI(id:String,password:String) {
        var x:Int!
        var b:Int!
        let semaphore = DispatchSemaphore (value: 0)

        let parameters = "{\"username\": \"\(id)\",\"password\": \"\(password)\",\"role\": \"customer\"}"
        let postData = parameters.data(using: .utf8)
   
        var request = URLRequest(url: URL(string: "http://land-surveyor-dev.eba-atp2ae9g.ap-southeast-1.elasticbeanstalk.com/api/v1/login")!,timeoutInterval: Double.infinity)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")

        request.httpMethod = "POST"
        request.httpBody = postData

         let task = URLSession.shared.dataTask(with: request) { data, response, error in
                 guard let data = data else {
                   print(String(describing: error))
                   return
                 }
                   do {
                       
                    let decoder = JSONDecoder()
                    let user =  try decoder.decode(User.self, from: data)
                    x = user.user_id
                   
                       if let httpResponse = response as? HTTPURLResponse {
                              b = httpResponse.statusCode
    
                           }
                   
                    
                    
                   }catch{
                       print(String(describing: error))
                       
                       
                   }
            
            semaphore.signal()
}
       
        
        task.resume()
        semaphore.wait()
       
        guard let status = b else{
            print("status weird")
            return
        }
        print(status)
        //print(status)
        if status == 200 {
        print("Login Sucessfully (Presenter) User_id = \(x!)")
        
        delegate.UserSuccessfullyLogin(id:x!)
 
        }
        else {
            delegate.UserUnsuccessfullyLogin()
        }
    }
}
