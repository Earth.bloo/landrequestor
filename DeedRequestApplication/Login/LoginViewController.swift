//
//  LoginViewController.swift
//  DeedRequestApplication
//
//  Created by admin on 17/2/2563 BE.
//  Copyright © 2563 EarthBlooProduction. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController,VCDelegate {
  
    
   

   
    @IBOutlet weak var signInbutton: UIButton!
    
    @IBOutlet weak var message: UILabel!
    
    var user_id:Int!
    @IBOutlet weak var IDTextField: UITextField!
    @IBOutlet weak var PWTextField: UITextField!
    var presenter:LoginPresenter!
    override func viewDidLoad() {
        super.viewDidLoad()
        //UserDefaults.standard.set(false, forKey: "IsLoginIn")
        if UserDefaults.standard.bool(forKey: "IsLoginIn") == true {
            let listvc = self.storyboard?.instantiateViewController(withIdentifier: "TabbarController") as! UITabBarController
            self.navigationController?.pushViewController(listvc, animated: false)
        }
        setUI()
        presenter = LoginPresenter()
        presenter.delegate = self
        message.isHidden = true
        IDTextField.text = "joelnwza"
        PWTextField.text = "123456"
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        // Hide the navigation bar on the this view controller
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        self.navigationItem.leftBarButtonItem = nil
    }
    
    override func viewWillDisappear(_ animated: Bool) {
           super.viewWillAppear(animated)

           // Hide the navigation bar on the this view controller
           self.navigationController?.setNavigationBarHidden(false, animated: animated)
       }


    

    @IBAction func ClickButton(_ sender: Any) {
        
      presenter.validateLogin(id: IDTextField.text!, password: PWTextField.text!)

    }
   
    
    
//delegate
    
    func showMessage(message: String) {
        self.message.isHidden = false
        self.message.textColor = UIColor.red
        self.message.text = message
       }
       
    
    
     func UserSuccessfullyLogin(id: Int) {
    
       UserDefaults.standard.set(true, forKey: "IsLoginIn")
        UserDefaults.standard.set(id, forKey: "User_id")
        user_id = UserDefaults.standard.integer(forKey: "User_id")
        let listvc = self.storyboard?.instantiateViewController(withIdentifier: "TabbarController") as! UITabBarController
                   self.navigationController?.pushViewController(listvc, animated: false)
    }
    
    func UserUnsuccessfullyLogin(){
        
            let alert = UIAlertController(title: "Incorrect Username or Password", message: "Please try again", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title:"OK",style: .default,handler: nil))
            present(alert,animated:true)
            
    }
    
    
      func setUI(){
        self.signInbutton.layer.cornerRadius = 10.0
        IDTextField.backgroundColor = UIColor.white
        IDTextField.layer.borderWidth = 1
        IDTextField.layer.borderColor = UIColor.rgb(red: 28, green: 87, blue: 154).cgColor
        IDTextField.layer.cornerRadius = 12
        
        PWTextField.backgroundColor = UIColor.white
        PWTextField.layer.borderWidth = 1
        PWTextField.layer.borderColor = UIColor.rgb(red: 28, green: 87, blue: 154).cgColor
        PWTextField.layer.cornerRadius = 12
        signInbutton.backgroundColor = UIColor.white
        signInbutton.layer.borderWidth = 2
        signInbutton.layer.borderColor = UIColor.rgb(red: 28, green: 87, blue: 154).cgColor
        signInbutton.setTitle("Log In",for: .normal)
        signInbutton.setTitleColor(UIColor.rgb(red: 28, green: 87, blue: 154), for: .normal)
        
      }
        
/// Listen segue calling
       
    
   
    
   
        // Check segue identifier
        
        

        }
    

