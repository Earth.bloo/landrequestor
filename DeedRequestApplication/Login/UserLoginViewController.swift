//
//  UserLoginViewController.swift
//  DeedRequestApplication
//
//  Created by admin on 11/4/2563 BE.
//  Copyright © 2563 EarthBlooProduction. All rights reserved.
//

import UIKit
import Lottie
import MaterialComponents.MaterialTextFields

class UserLoginViewController: UIViewController,VCDelegate {
    
    
    var animatedView:AnimationView = {
        let view = AnimationView()
        view.animation = Animation.named("logo")
        view.contentMode = .scaleAspectFit
        view.loopMode = .loop
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var Logintitle:UILabel = {
        let view = UILabel()
        view.text = "LAND REQUESTOR"
        view.font = UIFont(name: "AirbnbCerealApp-Bold", size: 35)
        view.textColor = colour.deepblue
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
        
    }()
    
    var UserTextField:MDCTextField = {
           let view = MDCTextField()
           view.placeholder = "Username"
           
           view.font = UIFont(name: "AirbnbCerealApp-Book", size: 20)
           view.translatesAutoresizingMaskIntoConstraints = false
           return view
       }()
    var PWTextField:MDCTextField = {
             let view = MDCTextField()
             view.placeholder = "Password"
             
             view.font = UIFont(name: "AirbnbCerealApp-Book", size: 20)
             view.isSecureTextEntry = true
             view.translatesAutoresizingMaskIntoConstraints = false
             return view
         }()
    var LoginButton :UIButton = {
              let view = UIButton()
             
             view.layer.backgroundColor = colour.deepblue.cgColor
              view.setTitle("Login", for: UIControl.State.normal)
              view.titleLabel?.font = UIFont(name: "AirbnbCerealApp-Light",size: 18)
              view.setTitleColor(.white, for: UIControl.State.normal)
              view.layer.cornerRadius = 20
              view.translatesAutoresizingMaskIntoConstraints = false
              view.addTarget(self, action: #selector(ClickButton), for: .touchUpInside)
              return view
          
    }()
     var ForgetLabel:UIButton = {
                 let view = UIButton()
                 view.setTitle("Forgot Password?", for: UIControl.State.normal)
                 view.titleLabel?.font = UIFont(name: "AirbnbCerealApp-Book",size: 15)
                 view.setTitleColor(colour.blue, for: UIControl.State.normal)
                 view.translatesAutoresizingMaskIntoConstraints = false
                 //view.addTarget(self, action: nil, for: .touchUpInside)
                 return view
                 
             }()
    
    var accountlabel:UILabel = {
           let view = UILabel()
           view.text = "Don't have account?"
           view.font = UIFont(name: "AirbnbCerealApp-Book", size: 16)
           view.textColor = UIColor.gray
           view.translatesAutoresizingMaskIntoConstraints = false
           return view
           
       }()
    var registerButton:UIButton = {
              let view = UIButton()
              view.setTitle("REGISTER", for: UIControl.State.normal)
              view.titleLabel?.font = UIFont(name: "AirbnbCerealApp-Book",size: 17)
              view.setTitleColor(colour.deepblue, for: UIControl.State.normal)
              view.translatesAutoresizingMaskIntoConstraints = false
              view.addTarget(self, action: #selector(registerButton(_:)), for: .touchUpInside)
              return view
              
          }()
 
    
    var textcontroller1:MDCTextInputControllerOutlined!
    var textcontroller2:MDCTextInputControllerOutlined!
    
    
     var presenter:LoginPresenter!
     var user_id:Int!

    override func viewDidLoad() {
        super.viewDidLoad()
        if UserDefaults.standard.bool(forKey: "IsLoginIn") == true {
                   let listvc = self.storyboard?.instantiateViewController(withIdentifier: "TabbarController") as! UITabBarController
                   self.navigationController?.pushViewController(listvc, animated: false)
                    appConfig.shared.fetchConfig(user_id: UserDefaults.standard.integer(forKey: "User_id"))
        
               }
        presenter = LoginPresenter()
        presenter.delegate = self
       // message.isHideen = true
        UserTextField.text = "joelnwza"
        PWTextField.text = "123456"
        setupUI()
        animatedView.play()

    }
    
    
     override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(animated)

            // Hide the navigation bar on the this view controller
            self.navigationController?.setNavigationBarHidden(true, animated: animated)
            self.navigationItem.leftBarButtonItem = nil
        }
        
        override func viewWillDisappear(_ animated: Bool) {
               super.viewWillAppear(animated)

               // Hide the navigation bar on the this view controller
               self.navigationController?.setNavigationBarHidden(false, animated: animated)
           }


        

        @objc func ClickButton(_ sender: Any) {
            
          presenter.validateLogin(id: UserTextField.text!, password: PWTextField.text!)

        }
    
       @objc func registerButton(_ sender: Any) {
      
        let vc = RegisterViewController()
            vc.modalPresentationStyle = .fullScreen
            vc.modalTransitionStyle = .crossDissolve
            self.present(vc, animated: true, completion: nil)
     }
       
        
        
    //delegate
        
        func showMessage(message: String) {
//            self.message.isHidden = false
//            self.message.textColor = UIColor.red
//            self.message.text = message
           }
           
        
        
         func UserSuccessfullyLogin(id: Int) {
        
           UserDefaults.standard.set(true, forKey: "IsLoginIn")
            UserDefaults.standard.set(id, forKey: "User_id")
            user_id = UserDefaults.standard.integer(forKey: "User_id")
            appConfig.shared.fetchConfig(user_id: user_id)
            let listvc = self.storyboard?.instantiateViewController(withIdentifier: "TabbarController") as! UITabBarController
             self.navigationController?.pushViewController(listvc, animated: false)
        }
        
        func UserUnsuccessfullyLogin(){
            
                let alert = UIAlertController(title: "Incorrect Username or Password", message: "Please try again", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title:"OK",style: .default,handler: nil))
                present(alert,animated:true)
                
        }
    
    
    func setupUI(){
        
     
         

        view.addSubview(animatedView)
        animatedView.topAnchor.constraint(equalTo: view.topAnchor, constant:30).isActive = true
        animatedView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        animatedView.heightAnchor.constraint(equalTo: view.heightAnchor,multiplier:0.40).isActive = true
        animatedView.widthAnchor.constraint(equalTo: view.widthAnchor,multiplier:0.7).isActive = true
        
        view.addSubview(Logintitle)
        Logintitle.topAnchor.constraint(equalTo: animatedView.bottomAnchor, constant: -80).isActive = true
        Logintitle.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        Logintitle.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        
        view.addSubview(UserTextField)
        UserTextField.topAnchor.constraint(equalTo: Logintitle.bottomAnchor, constant: 30).isActive = true
        UserTextField.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 60).isActive = true
        UserTextField.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -60).isActive = true
       // UserTextField.heightAnchor.constraint(equalToConstant: 0).isActive = true
        self.textcontroller1 = MDCTextInputControllerOutlined(textInput: UserTextField)
        self.textcontroller1.activeColor = colour.deepblue
        self.textcontroller1.normalColor = colour.deepblue
        self.textcontroller1.inlinePlaceholderFont =  UIFont(name: "AirbnbCerealApp-Book", size: 17)
//
        view.addSubview(PWTextField)
        PWTextField.topAnchor.constraint(equalTo: UserTextField.bottomAnchor, constant: 5).isActive = true
        PWTextField.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 60).isActive = true
        PWTextField.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -60).isActive = true
        //PWTextField.widthAnchor.constraint(equalToConstant: 300).isActive = true
        self.textcontroller2 = MDCTextInputControllerOutlined(textInput: PWTextField)
        self.textcontroller2.activeColor = colour.deepblue
        self.textcontroller2.normalColor = colour.deepblue
        self.textcontroller2.inlinePlaceholderFont =  UIFont(name: "AirbnbCerealApp-Book", size: 17)
        
        view.addSubview(LoginButton)
        LoginButton.topAnchor.constraint(equalTo: PWTextField.bottomAnchor, constant: 27).isActive = true
        LoginButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        LoginButton.widthAnchor.constraint(equalToConstant: 150).isActive = true
        LoginButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        view.addSubview(accountlabel)
       accountlabel.topAnchor.constraint(equalTo: LoginButton.bottomAnchor, constant: 20).isActive = true
       accountlabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 80).isActive = true
        view.addSubview(registerButton)
        registerButton.topAnchor.constraint(equalTo: LoginButton.bottomAnchor, constant: 13).isActive = true
        registerButton.leadingAnchor.constraint(equalTo: accountlabel.trailingAnchor, constant: 5).isActive = true
        
      view.addSubview(ForgetLabel)
       ForgetLabel.topAnchor.constraint(equalTo: PWTextField.bottomAnchor, constant: -15).isActive = true
       ForgetLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 60).isActive = true
        
        
       
        
        
        
        
    }
    
    
    

   

}

