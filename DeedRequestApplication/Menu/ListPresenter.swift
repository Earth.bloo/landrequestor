//
//  ListPresenter.swift
//  DeedRequestApplication
//
//  Created by admin on 22/2/2563 BE.
//  Copyright © 2563 EarthBlooProduction. All rights reserved.
//

import UIKit
import Foundation

protocol LVCDelegate:AnyObject {
    func setTable(number:[AllDeed])
    
}

class ListPresenter: NSObject {
   weak var delegate:LVCDelegate!
    var deed:[AllDeed]!
   
    
    func callAPI(userID:Int,status:String){
         let url = "http://land-surveyor-dev.eba-atp2ae9g.ap-southeast-1.elasticbeanstalk.com/api/v1/deeds?customer_id=\(userID)\(status)"
        
        var semaphore = DispatchSemaphore (value: 0)
        var request = URLRequest(url: URL(string: url)!,timeoutInterval: Double.infinity)
        request.httpMethod = "GET"

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
          guard let data = data else {
            print(String(describing: error))
            return
          }
            do {
                if let httpResponse = response as? HTTPURLResponse {
                 let x = httpResponse.statusCode
                print(x)
                    if (x == 200){
                let decoder = JSONDecoder()
                self.deed =  try decoder.decode([AllDeed]?.self, from: data)
                
                    }
                    if (x == 404){
                        self.deed = []
                    }
                                       }
                
               
               
                
             self.delegate.setTable(number:self.deed)
            }catch{
                print(String(describing: error))
                
            }
          semaphore.signal()
        }

        task.resume()
        semaphore.wait()
    }
    
    func cancleOrder(x:Int){
        print("delete(deed_id)==== \(x)")
        var semaphore = DispatchSemaphore (value: 0)

        let parameters = "{\n\t\"status_id\": 5\n}"
        let postData = parameters.data(using: .utf8)

        var request = URLRequest(url: URL(string: "http://land-surveyor-dev.eba-atp2ae9g.ap-southeast-1.elasticbeanstalk.com/api/v1/deeds/\(x)")!,timeoutInterval: Double.infinity)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")

        request.httpMethod = "PATCH"
        request.httpBody = postData

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
          guard let data = data else {
            print(String(describing: error))
            return
          }
            print(String(decoding: data, as: UTF8.self))
        
          semaphore.signal()
        }

        task.resume()
        semaphore.wait()
        
    }
    
    
    
    
   
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
}
