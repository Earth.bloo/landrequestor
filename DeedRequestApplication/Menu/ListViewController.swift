//
//  ListViewController.swift
//  DeedRequestApplication
//
//  Created by admin on 22/2/2563 BE.
//  Copyright © 2563 EarthBlooProduction. All rights reserved.
//

import UIKit
import SkeletonView

class ListViewController: UIViewController,LVCDelegate {
    

    
    
    @IBOutlet weak var addRequestButton: UIButton!
    @IBOutlet weak var LogoutButton: UIButton!
    
    
    //POPUP BUTTON
    @IBOutlet weak var FilterButton: UIButton!
    @IBOutlet weak var AllButton: UIButton!
    @IBOutlet weak var WaitingButton: UIButton!
    @IBOutlet weak var AssignedButton: UIButton!
    @IBOutlet weak var CollectingButton: UIButton!
    @IBOutlet weak var SuccessButton: UIButton!
    @IBOutlet var filterPopUp: UIView!
    var  transparentBackground:UIView!
   
    @IBOutlet weak var FilterTableView: UITableView!
    @IBOutlet weak var CancelButton: UIButton!
    
    var message :String!
    var userID:Int = UserDefaults.standard.integer(forKey: "User_id")
    var count = 0
    
    @IBOutlet weak var labelName: UILabel!
    
    var presenter:ListPresenter!
    var tmpdeed:[AllDeed]!
    
    @IBOutlet weak var TableView: UITableView!
    var refreshControll = UIRefreshControl()
    var currentStatus:String = "not:5"
    var goView:Int!
    
   
    
    
    //Config
    var texttitle:String!
    var status1:String!
    var status2:String!
    var status3:String!
    var status4:String!
    var status:[StatusCon]!
    
  
    
    
    override func viewDidLoad() {
       
        setConfig()
        userID = UserDefaults.standard.integer(forKey: "User_id")
        setUI()
        print("ListViewCOntroller recieve \(userID) form Login")
        super.viewDidLoad()
        presenter = ListPresenter()
        presenter.delegate = self
        APIStatus()
        TableView.dataSource = self
        TableView.delegate = self
        FilterTableView.dataSource = self
        FilterTableView.delegate = self
        TableView.tag = 0
        FilterTableView.tag = 1
        self.TableView.refreshControl = refreshControll
        self.refreshControll.addTarget(self, action: #selector(self.initPullRefresh(_:)), for: .valueChanged)
        self.navigationItem.setHidesBackButton(true, animated: false)
      
       
        
    }
    
  
    func setConfig(){
        status = appConfig.shared.con.resources.status
        texttitle = appConfig.shared.con.app_name
        status1 = appConfig.shared.con.resources.status[0].name
        status2 = appConfig.shared.con.resources.status[1].name
        status3 = appConfig.shared.con.resources.status[2].name
        status4 = appConfig.shared.con.resources.status[3].name
    }
    
    func mapValue(i:Int) -> String {
        let statusCon = appConfig.shared.con.resources.status!
        let st1 = statusCon.filter { status in status.status_id == i }.map { status in status.name }
        guard let st = st1[0] else{
            return ""
        }
        return st
    }
    
    override func viewDidAppear(_ animated: Bool) {
         self.tabBarController?.title = "Title"
               self.tabBarController?.navigationItem.hidesBackButton = true
              //navigation Bar
               let titlelabel = UILabel(frame: CGRect(x: 0, y: 0, width: view.frame.width , height: view.frame.height))
               titlelabel.text  = texttitle
               
               titlelabel.font = UIFont(name:"AirbnbCerealApp-Bold",size: 23)
               titlelabel.textColor = UIColor.white
               self.tabBarController?.navigationItem.titleView = titlelabel
    }
    
    
    
    
    
   
    
   
    

    
    
    @objc func initPullRefresh(_ refresh: UIRefreshControl) {
            
           let delayTime = DispatchTime.now() + 2
           DispatchQueue.main.asyncAfter(deadline: delayTime) {
               
           
               self.count = self.tmpdeed.count
               
            self.APIStatus(status:self.currentStatus)
               self.TableView.reloadData()
               refresh.endRefreshing()
               
        }
       
       }
    
    func setUI(){
          // FilterButton.setDynamicFontSize()
        CancelButton.alpha = 0
           FilterButton.frame = CGRect(x: 0, y: 0, width: 80, height: 0)
           let Userbutton = UIButton(type: .system)
           
           Userbutton.setImage(UIImage(named:"notification")?.withRenderingMode(.alwaysOriginal), for: .normal)
          
           Userbutton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
           self.tabBarController?.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: Userbutton)
       
         
            let nibName = UINib(nibName: "TableViewCell", bundle: nil)
           TableView.register(nibName,forCellReuseIdentifier: "nibtablecell")
           
            FilterButton.layer.cornerRadius = 20
            FilterButton.layer.borderWidth = 0.7
            FilterButton.layer.borderColor = UIColor.black.cgColor
            
//            WaitingButton.setTitle(status1, for: UIControl.State.normal)
//            CollectingButton.setTitle(status2, for: UIControl.State.normal)
//            AssignedButton.setTitle(status3, for: UIControl.State.normal)
//            SuccessButton.setTitle(status4, for: UIControl.State.normal)
          
           
           
           
          
       }
    
   
 
    

            // Initial setup for image for Large NavBar state since the the screen always has Large NavBar once it gets opened
       
    }
    
    
  
    
extension ListViewController: UITableViewDataSource,UITableViewDelegate {
    

   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if tableView.tag == 0 {
    return(tmpdeed.count)
    }
     if tableView.tag == 1 {
        return(status.count)
   }
    return 0
    }
    
   
   
   
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView.tag == 0 {
        let cell = tableView.dequeueReusableCell(withIdentifier: "nibtablecell",for:indexPath) as! TableViewCell
        let status =  mapValue(i: tmpdeed[indexPath.row].status_id)
        cell.commonInit(deed_name:tmpdeed[indexPath.row].deed_name!, address:tmpdeed[indexPath.row].address!,status: status,date:tmpdeed[indexPath.row].created_date!)

            return cell
        }
        
       if tableView.tag == 1 {
             
             let cell2 = FilterTableView.dequeueReusableCell(withIdentifier: "filtercell",for:indexPath)
        cell2.textLabel?.text = status[indexPath.row].name
            cell2.textLabel?.textAlignment = .center
        tableView.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: UIScreen.main.bounds.width)
      

            return cell2
        }
            return UITableViewCell()
   }
   
    
   
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView.tag == 0 {
        return 160
        }
        if tableView.tag == 1 {
            return 60
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        if tableView.tag == 0 {
        goView = tmpdeed![indexPath.row].deed_id
        let goViewSTR = String(goView)
//        let listvc = self.storyboard?.instantiateViewController(withIdentifier: "DeedDetailViewController") as! DeedDetailViewController
//                              self.navigationController?.pushViewController(listvc, animated: true)
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "DeedDetailViewController") as! DeedDetailViewController
        vc.deed_id = goViewSTR
            
        vc.modalPresentationStyle = .fullScreen
        vc.modalTransitionStyle = .crossDissolve
        self.present(vc, animated: true, completion: nil)
        }
        if tableView.tag == 1 {
           let strStatus =  String(status[indexPath.row].status_id)
            currentStatus = strStatus
            APIStatus(status:strStatus)
            CancelButton.alpha = 1
            
            
         
            FilterButton.setTitle(status[indexPath.row].name, for: .normal)
            FilterButton.frame = CGRect(x: 0, y: 0, width: 150, height: 0)
            animatedOut(desiredView:transparentBackground)
        }
            
        }
    
    
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
          //config statud
        if tableView.tag == 0 {
        let status = mapValue(i:tmpdeed[indexPath.row].status_id)
        
          if editingStyle == .delete {
            if status !=  status3 {
                if status != status4 {
                    
                    self.deleteData(index: indexPath.row)
                }else {
                    let alert = UIAlertController(title: "Alert", message: "The request have been \(status4!)", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title:"Ok",style: .default,handler:nil))
                    present(alert,animated:true)
                    
                }
            }else {
                let alert = UIAlertController(title: "Alert", message: "The request have been \(status3!)", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title:"Ok",style: .default,handler:nil))
                present(alert,animated:true)
                
            }
          }
        }
          
      }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
           return 40
       }
       func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if tableView.tag == 0 {
           let headerview = Bundle.main.loadNibNamed("HeaderView", owner: self, options: nil)?.first as! HeaderView
           
        headerview.summary(sum:tmpdeed.count)
           
          
          
           return headerview
        }
           return HeaderView()
       }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
         let rotationTransform = CATransform3DTranslate(CATransform3DIdentity, 0, 40 , 0)
        cell.layer.transform = rotationTransform
        cell.alpha = 0
        UIView.animate(withDuration: 1.0){
            cell.layer.transform = CATransform3DIdentity
            cell.alpha = 1.0
        }
    }
       
        
    
   
    
   func deleteData(index: Int) {
        
        let alert = UIAlertController(title: "Delete", message: "Do you want to cancel this?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title:"No",style: .cancel,handler: nil))
        alert.addAction(UIAlertAction(title:"Yes",style: .default,handler: {
            (action:UIAlertAction) in
            self.TableView.beginUpdates()
            self.presenter.cancleOrder(x:self.tmpdeed![index].deed_id!)
            self.tmpdeed.remove(at: index)
            let indexPath = IndexPath(row: index, section: 0)
            self.TableView.deleteRows(at: [indexPath], with: UITableView.RowAnimation.fade)
            self.TableView.endUpdates()
            self.TableView.reloadData()
            
        }))
        
        present(alert,animated:true)
      

        //print(tmpdeed![indexPath.row].deed_name)
            }
    
    
    //delegate
    
    func setTable(number: [AllDeed]) {
        self.tmpdeed = number
        
    }
    
   
    
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
            guard let showViewController = segue.destination as? DeedDetailViewController else {
                return
            }
        guard let goViweOp = goView else{
            print("nothing in goview")
            return
        }
        showViewController.deed_id = String(goViweOp)
        print("call show page view")
        }
    
    
    
    
    
    
   
    

    
    
    //

        
    
}


// pop up


extension ListViewController{
    
    
   @IBAction func clickCancelButton(_ sender: Any) {
    CancelButton.alpha = 0
     FilterButton.setTitle("Filter", for: .normal)
    currentStatus = "not:5"
    APIStatus(status: currentStatus)
    FilterTableView.reloadData()
    
     }
     
    
     @IBAction func clickFilter(_ sender:
     UIButton) {
        sender.pulse()
        
        
        animatedIn(desiredView:filterPopUp)
        
    }
//     @IBAction func selectedAll(_ sender:  UIButton) {
//        sender.pulse()
//        setColorUnselected()
//        AllButton.setTitleColor(colour.blue, for: UIControl.State.normal)
//        FilterButton.setTitle("Filter", for: .normal)
//        FilterButton.frame = CGRect(x: 0, y: 0, width: 80, height: 0)
//        currentStatus = 0
//        APIStatus(status:currentStatus)
//        self.TableView.reloadData()
//        animatedOut(desiredView:transparentBackground)
//    }
//
//    @IBAction func selectedWaiting(_ sender: UIButton) {
//        sender.pulse()
//        setColorUnselected()
//        WaitingButton.setTitleColor(colour.blue, for: UIControl.State.normal)
//        FilterButton.setTitle(appConfig.shared.con.resources.status[0].name, for: .normal)
//        FilterButton.frame = CGRect(x: 0, y: 0, width: 100, height: 0)
//         currentStatus = 1
//          APIStatus(status:currentStatus)
//        self.TableView.reloadData()
//        animatedOut(desiredView:transparentBackground)
//
//       }
//    @IBAction func selectedAssigned(_ sender: UIButton) {
//        sender.pulse()
//        setColorUnselected()
//        AssignedButton.setTitleColor(colour.blue, for: UIControl.State.normal)
//        FilterButton.setTitle(appConfig.shared.con.resources.status[1].name, for: .normal)
//        FilterButton.frame = CGRect(x: 0, y: 0, width: 150, height: 0)
//        currentStatus = 2
//         APIStatus(status:currentStatus)
//        self.TableView.reloadData()
//         animatedOut(desiredView:transparentBackground)
//       }
//    @IBAction func selectedCollecting(_ sender: UIButton) {
//        sender.pulse()
//        setColorUnselected()
//        CollectingButton.setTitleColor(colour.blue, for: UIControl.State.normal)
//        FilterButton.setTitle(appConfig.shared.con.resources.status[2].name, for: .normal)
//        FilterButton.frame = CGRect(x: 0, y: 0, width: 150, height: 0)
//        currentStatus = 3
//         APIStatus(status:currentStatus)
//        self.TableView.reloadData()
//         animatedOut(desiredView:transparentBackground)
//       }
//    @IBAction func selectedSuccess(_ sender: UIButton) {
//        sender.pulse()
//        setColorUnselected()
//        SuccessButton.setTitleColor(colour.blue, for: UIControl.State.normal)
//        FilterButton.setTitle(appConfig.shared.con.resources.status[3].name, for: .normal)
//        FilterButton.frame = CGRect(x: 0, y: 0, width: 150, height: 0)
//        currentStatus = 4
//           APIStatus(status:currentStatus)
//        self.TableView.reloadData()
//         animatedOut(desiredView:transparentBackground)
//       }
    @IBAction func exitFilter(_ sender: Any) {
        animatedOut(desiredView:transparentBackground)
    }
    
    func setColorUnselected() {
         AllButton.setTitleColor(UIColor.black, for: UIControl.State.normal)
         WaitingButton.setTitleColor(UIColor.black, for: UIControl.State.normal)
         AssignedButton.setTitleColor(UIColor.black, for: UIControl.State.normal)
         CollectingButton.setTitleColor(UIColor.black, for: UIControl.State.normal)
         SuccessButton.setTitleColor(UIColor.black, for: UIControl.State.normal)
        
    }

    func setPopUp(){
       
        transparentBackground = UIView(frame: UIScreen.main.bounds)
                         self.transparentBackground.backgroundColor = UIColor(white: 0.0, alpha: 0.54)
      //  UIApplication.shared.keyWindow!.addSubview(self.transparentBackground)
        
       
        filterPopUp.bounds = CGRect(x:0,y:0,width:self.view.bounds.width * 0.9,height: self.view.bounds.height * 0.4)
  
    }
    
    func animatedIn(desiredView:UIView){
        setPopUp()
        let backgroundview = self.view!
        UIApplication.shared.keyWindow!.addSubview(self.transparentBackground)
        self.transparentBackground.addSubview(desiredView)
//        UIApplication.shared.keyWindow!.bringSubviewToFront(self.transparentBackground)
//        self.view.bringSubviewToFront(transparentBackground)
        
        desiredView.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
        desiredView.center = backgroundview.center
        
        desiredView.layer.cornerRadius = 20
        //        blur.alpha = 0.6
        
        
        UIView.animate(withDuration: 0.3, animations: {
            self.transparentBackground.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            desiredView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            desiredView.alpha = 1
            
        })
    }
        
    func animatedOut(desiredView:UIView){
            
            UIView.animate(withDuration: 0.1, animations: {
                       desiredView.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
                      desiredView.alpha = 0
                       
                   })
             UIApplication.shared.keyWindow!.willRemoveSubview(self.transparentBackground)
            
        }
    
    func APIStatus(status:String = "not:5"){
        
        presenter.callAPI(userID:self.userID,status:"&status_id=\(status)&sort=status_id,updated_date")
            self.TableView.reloadData()
                   
               
//                presenter.callAPI(userID:self.userID,status: "&status_id=not:5&sort=status_id,updated_date")
               
    }
    
//    override func viewWillAppear(_ animated: Bool) {
//          let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
//          statusBar.backgroundColor = UIColor.init(red: 39/250, green: 109/250, blue: 171/250, alpha: 1)
//          UIApplication.shared.keyWindow?.addSubview(statusBar)
//         
//    }
        
    
    
    
    
    
    
    
    
}






