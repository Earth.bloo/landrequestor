//
//  PhotoCollectionViewCell.swift
//  DeedRequestApplication
//
//  Created by admin on 28/3/2563 BE.
//  Copyright © 2563 EarthBlooProduction. All rights reserved.
//

import UIKit

class PhotoCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var ImageViewCell: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setup(image:UIImage){
        ImageViewCell.image = image
    }

}
