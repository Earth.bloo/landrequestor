//
//  HeaderControllerTableViewCell.swift
//  DeedRequestApplication
//
//  Created by admin on 5/3/2563 BE.
//  Copyright © 2563 EarthBlooProduction. All rights reserved.
//

import UIKit



class HeaderView: UITableViewCell {

    @IBOutlet weak var summaryLabel: UILabel!
    @IBOutlet weak var StatusLabel: UILabel!
    @IBOutlet weak var NameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    
        
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func summary(sum:Int){
        self.summaryLabel.text = String(sum)
        
    }
    
    
    

    
    
    
}
