//
//  TableViewCell.swift
//  DeedRequestApplication
//
//  Created by admin on 14/3/2563 BE.
//  Copyright © 2563 EarthBlooProduction. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {

 
    
    @IBOutlet weak var cellView: UIView!
    
    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var deed_nameLabel: UILabel!

    @IBOutlet weak var AddressLabel: UILabel!
    @IBOutlet weak var StatusLabel: UILabel!
    @IBOutlet weak var CreatedDateLabel: UILabel!
    var status1:String!
    var status2:String!
    var status3:String!
    var status4:String!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
       setConfig()
        
        cellView.layer.cornerRadius = 5
        statusView.layer.cornerRadius = 5
        cellView.layer.shadowColor = UIColor.gray.cgColor
        cellView.layer.shadowOpacity = 0.3
        cellView.layer.shadowOffset = .zero
        cellView.layer.shadowRadius = 3
        StatusLabel.layer.borderWidth = 1.0
        StatusLabel.layer.borderColor = UIColor.rgb(red: 153, green: 157, blue: 160,alpha:1).cgColor
        StatusLabel.layer.cornerRadius = 14
        
    }
    
    func setConfig(){
             
             status1 = appConfig.shared.con.resources.status[0].name
             status2 = appConfig.shared.con.resources.status[1].name
             status3 = appConfig.shared.con.resources.status[2].name
             status4 = appConfig.shared.con.resources.status[3].name
            print("\(status1),\(status2),\(status3),\(status4)")
         }
    
  

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func commonInit(deed_name:String,address:String,status:String,date:String){
        deed_nameLabel.text = deed_name.uppercased()
        AddressLabel.text = address
        StatusLabel.text = status
        
        var dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "E, dd MMM yyyy HH:mm:ss z"// yyyy-MM-dd"
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "EE, dd MMM yyyy"
        let dataString = dateFormatter.string(from: date!)
        CreatedDateLabel.text = dataString
        
        switch status {
        case status1:
            self.statusView.backgroundColor = UIColor.rgb(red: 170, green: 170, blue: 170,alpha:1)
        case status3:
            self.statusView.backgroundColor = UIColor.rgb(red: 245, green: 193, blue: 31,alpha:1)
        case status2:
            self.statusView.backgroundColor = UIColor.rgb(red: 24, green: 144, blue: 255,alpha:1)
        case status4:
            self.statusView.backgroundColor = UIColor.rgb(red:82,green:196,blue:26,alpha:1)
            
        default:
            self.statusView.backgroundColor = UIColor.rgb(red: 255, green: 255, blue: 255,alpha:1)
        }
    }
    
}
