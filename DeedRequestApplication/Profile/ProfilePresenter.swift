//
//  File.swift
//  DeedRequestApplication
//
//  Created by admin on 8/4/2563 BE.
//  Copyright © 2563 EarthBlooProduction. All rights reserved.
//
import UIKit
import Foundation

protocol setProfileDelegate{
    func sendback(profile:Profile)
}
class ProfilePresenter: NSObject {
    var profile:Profile!
    var delegate:setProfileDelegate!
    
     func callAPI(user_id:Int){
            
            var semaphore = DispatchSemaphore (value: 0)
            var request = URLRequest(url: URL(string: "http://land-surveyor-dev.eba-atp2ae9g.ap-southeast-1.elasticbeanstalk.com/api/v1/customers?user_id=\(user_id)")!,timeoutInterval: Double.infinity)
            request.httpMethod = "GET"

            let task = URLSession.shared.dataTask(with: request) { data, response, error in
              guard let data = data else {
                print(String(describing: error))
                return
              }
                do {
                    let decoder = JSONDecoder()
                    self.profile =  try decoder.decode(Profile.self, from: data)
                    
                 
                }catch{
                    print(String(describing: error))
                    
                }
              semaphore.signal()
            }

            task.resume()
            semaphore.wait()
        delegate.sendback(profile: self.profile)
        }


}
