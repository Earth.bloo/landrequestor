//
//  ProfileViewController.swift
//  DeedRequestApplication
//
//  Created by admin on 18/3/2563 BE.
//  Copyright © 2563 EarthBlooProduction. All rights reserved.
//

import UIKit
import MaterialComponents.MaterialTextFields

class ProfileViewController:
UIViewController,setProfileDelegate {
    
    @IBOutlet weak var FirstNameField: MDCTextField!
    @IBOutlet weak var LastNameField: MDCTextField!
    @IBOutlet weak var PhoneNumberField: MDCTextField!
    @IBOutlet weak var LogoutButton:UIButton!
    var profile:Profile!
    func sendback(profile: Profile) {
        
        
        FirstNameField.text = profile.first_name
        LastNameField.text = profile.last_name
        PhoneNumberField.text = profile.phone_number
    }
    
    var userID:Int = UserDefaults.standard.integer(forKey: "User_id")
    var presenter:ProfilePresenter!

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = ProfilePresenter()
        presenter.delegate = self
        presenter.callAPI(user_id: userID)
        setUI()

        // Do any additional setup after loading the view.
    }
    
    func setUI(){
               LogoutButton.layer.borderWidth = 1
               LogoutButton.layer.borderColor = UIColor.red.cgColor
             LogoutButton.setTitleColor(.red, for: UIControl.State.normal)
               LogoutButton.layer.cornerRadius = 20
    }
    
    @IBAction func ClickLogout(_ sender: Any) { UserDefaults.standard.set(false, forKey: "IsLoginIn")
           let loginVC = storyboard?.instantiateViewController(identifier: "LoginViewController") as! LoginViewController
           self.navigationController?.pushViewController(loginVC, animated: false)
    }
    
    override func viewDidAppear(_ animated: Bool) {
         self.tabBarController?.title = "Title"
               self.tabBarController?.navigationItem.hidesBackButton = true
              //navigation Bar
               let titlelabel = UILabel(frame: CGRect(x: 0, y: 0, width: view.frame.width , height: view.frame.height))
               titlelabel.text  = "Profile"
               
               titlelabel.font = UIFont(name:"AirbnbCerealApp-Bold",size: 23)
               titlelabel.textColor = UIColor.white
               self.tabBarController?.navigationItem.titleView = titlelabel
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
