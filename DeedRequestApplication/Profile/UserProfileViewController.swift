//
//  UserProfileViewController.swift
//  DeedRequestApplication
//
//  Created by admin on 11/4/2563 BE.
//  Copyright © 2563 EarthBlooProduction. All rights reserved.
//

import UIKit
import Lottie
import MaterialComponents.MaterialTextFields

class UserProfileViewController: UIViewController,setProfileDelegate  {
    
    let container:UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    
    
    
    var animatedView:AnimationView = {
        let view = AnimationView()
        view.animation = Animation.named("walkingman")
        view.contentMode = .scaleAspectFill
        view.loopMode = .loop
        
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    var textcontroller1:MDCTextInputControllerOutlined!
    var textcontroller2:MDCTextInputControllerOutlined!
    var textcontroller3:MDCTextInputControllerOutlined!
    
    var firstnameTextField:MDCTextField = {
        let view = MDCTextField()
        view.placeholder = "First name"
        view.text = "Peerapat"
        view.font = UIFont(name: "AirbnbCerealApp-Book", size: 20)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var LastnameTextField:MDCTextField = {
        let view = MDCTextField()
        view.placeholder = "Last name"
        view.text = "Dechathitirat"
        view.font = UIFont(name: "AirbnbCerealApp-Book", size: 20)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var PNTextField:MDCTextField = {
        let view = MDCTextField()
        view.placeholder = "Phone Number"
        view.text = "0877588057"
        view.font = UIFont(name: "AirbnbCerealApp-Book", size: 20)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var logoutButton:UIButton = {
        let view = UIButton()
        view.layer.borderWidth = 1
        view.layer.borderColor = UIColor.red.cgColor
        view.setTitle("Logout", for: UIControl.State.normal)
        view.titleLabel?.font = UIFont(name: "AirbnbCerealApp-Light",size: 18)
        view.setTitleColor(.red, for: UIControl.State.normal)
        view.layer.cornerRadius = 20
        view.translatesAutoresizingMaskIntoConstraints = false
        view.addTarget(self, action: #selector(ClickLogout), for: .touchUpInside)
        return view
    }()
    
    var userID:Int = UserDefaults.standard.integer(forKey: "User_id")
    var presenter:ProfilePresenter!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
                   presenter = ProfilePresenter()
                   presenter.delegate = self
                   presenter.callAPI(user_id: userID)
         animatedView.play()
        
    }
    
    func setupUI(){
        view.addSubview(animatedView)
        animatedView.topAnchor.constraint(equalTo: view.topAnchor,constant: 70).isActive = true
        animatedView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        animatedView.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.3).isActive = true
       // animatedView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.7).isActive = true
       
        animatedView.AspectRation(1.0/1.0).isActive = true
        
        view.addSubview(firstnameTextField)
        firstnameTextField.topAnchor.constraint(equalTo: animatedView.bottomAnchor, constant: 20).isActive = true
        firstnameTextField.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 50).isActive = true
        firstnameTextField.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -50).isActive = true
        firstnameTextField.heightAnchor.constraint(equalToConstant: 70).isActive = true
        self.textcontroller1 = MDCTextInputControllerOutlined(textInput: firstnameTextField)
        // self.textcontroller.textInsets(UIEdgeInsets(top: 16, left: 16, bottom: 16, right: 16))
        self.textcontroller1.activeColor = colour.deepblue
        self.textcontroller1.normalColor = colour.deepblue
        self.textcontroller1.inlinePlaceholderFont =  UIFont(name: "AirbnbCerealApp-Book", size: 20)
        
        
        view.addSubview(LastnameTextField)
        LastnameTextField.topAnchor.constraint(equalTo: firstnameTextField.bottomAnchor, constant: 15).isActive = true
        LastnameTextField.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 50).isActive = true
        //    LastnameTextField.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 60).isActive = true
        LastnameTextField.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -50).isActive = true
        LastnameTextField.heightAnchor.constraint(equalToConstant: 70).isActive = true
        self.textcontroller2 = MDCTextInputControllerOutlined(textInput: LastnameTextField)
        //    self.textcontroller.textInsets(UIEdgeInsets(top: 16, left: 16, bottom: 16, right: 16))
        self.textcontroller2.activeColor = colour.deepblue
        self.textcontroller2.normalColor = colour.deepblue
        self.textcontroller2.inlinePlaceholderFont =  UIFont(name: "AirbnbCerealApp-Book", size: 20)
        
        view.addSubview(PNTextField)
        PNTextField.topAnchor.constraint(equalTo: LastnameTextField.bottomAnchor, constant:15).isActive = true
        PNTextField.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 50).isActive = true
        PNTextField.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -50).isActive = true
        PNTextField.heightAnchor.constraint(equalToConstant: 70).isActive = true
        self.textcontroller3 = MDCTextInputControllerOutlined(textInput: PNTextField)
        //  self.textcontroller.textInsets(UIEdgeInsets(top: 16, left: 16, bottom: 16, right: 16))
        self.textcontroller3.activeColor = colour.deepblue
        self.textcontroller3.normalColor = colour.deepblue
        self.textcontroller3.inlinePlaceholderFont =  UIFont(name: "AirbnbCerealApp-Book", size: 20)
        self.textcontroller3.inlinePlaceholderColor = colour.deepblue


        view.addSubview(logoutButton)
       logoutButton.topAnchor.constraint(equalTo: PNTextField.bottomAnchor, constant: 35).isActive = true
        logoutButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 80).isActive = true
        logoutButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -80).isActive = true
      // logoutButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0).isActive = true
        logoutButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        
        
        
        
        
        
    }
    
    
    @objc  func ClickLogout(_ sender: Any) { UserDefaults.standard.set(false, forKey: "IsLoginIn")
        let ui = UIAlertController(title: "Do you want to logout?", message: "Choose a Source", preferredStyle: .actionSheet)
               ui.addAction(UIAlertAction(title: "Logout", style: .default , handler: {(action:UIAlertAction) in
                let loginVC = self.storyboard?.instantiateViewController(identifier: "UserLoginViewController") as! UserLoginViewController
                    self.navigationController?.pushViewController(loginVC, animated: false)
               } ))
        ui.addAction(UIAlertAction(title: "Cancle", style: .cancel, handler: {(action:UIAlertAction) in } ))
               self.present(ui,animated: true, completion: nil)
        let loginVC = storyboard?.instantiateViewController(identifier: "UserLoginViewController") as! UserLoginViewController
        self.navigationController?.pushViewController(loginVC, animated: false)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.tabBarController?.title = "Title"
        self.tabBarController?.navigationItem.hidesBackButton = true
        //navigation Bar
        let titlelabel = UILabel(frame: CGRect(x: 0, y: 0, width: view.frame.width , height: view.frame.height))
        titlelabel.text  = "Profile"
        
        titlelabel.font = UIFont(name:"AirbnbCerealApp-Bold",size: 23)
        titlelabel.textColor = UIColor.white
        self.tabBarController?.navigationItem.titleView = titlelabel
    }
    
    
    func sendback(profile: Profile) {
        
        firstnameTextField.text = profile.first_name
        LastnameTextField.text = profile.last_name
        PNTextField.text = profile.phone_number
    }
    
    override func viewWillAppear(_ animated: Bool) {
         animatedView.play()
    }
    
}
