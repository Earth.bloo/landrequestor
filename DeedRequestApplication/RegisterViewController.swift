//
//  RegisterViewController.swift
//  DeedRequestApplication
//
//  Created by admin on 23/3/2563 BE.
//  Copyright © 2563 EarthBlooProduction. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController {
    
    let scrollView:UIScrollView = {
        let view = UIScrollView()
        view.contentSize.height = 2000
        view.backgroundColor = UIColor.white
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let tabView:UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.rgb(red: 28, green: 87, blue: 154,alpha:1)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let texttitle:UILabel = {
        let view = UILabel()
        view.text = "SIGN-UP"
        view.textColor = UIColor.white
        view.font = UIFont(name: "AirbnbCerealApp-Book",size: 40)
        
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let FNameLabel:UILabel = {
        let view = UILabel()
        view.text = "First Name"
        view.textColor = UIColor.black
        view.font = UIFont.systemFont(ofSize: 20)
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    let FNameTextField:UITextField = {
        let view = UITextField()
        view.borderStyle = .none
        view.placeholder = "Last Name"
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let FNameLine:UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.gray
        view.translatesAutoresizingMaskIntoConstraints = false
        return(view)
    }()
    
    let LNameLabel:UILabel = {
           let view = UILabel()
           view.text = "Last Name"
           view.textColor = UIColor.black
           view.font = UIFont.systemFont(ofSize: 20)
           view.translatesAutoresizingMaskIntoConstraints = false
           
           return view
       }()
    
    let LNameTextField:UITextField = {
        let view = UITextField()
        view.borderStyle = .none
        view.placeholder = "Last Name"
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let LNameLine:UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.gray
        view.translatesAutoresizingMaskIntoConstraints = false
        return(view)
    }()
    
    
    let PhoneNLabel:UILabel = {
           let view = UILabel()
           view.text = "Phone Number"
           view.textColor = UIColor.black
           view.font = UIFont.systemFont(ofSize: 20)
           view.translatesAutoresizingMaskIntoConstraints = false
           
           return view
       }()
    
    let PhoneNTextField:UITextField = {
        let view = UITextField()
        view.borderStyle = .none
        view.placeholder = "Phone Number"
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let PhoneNLine:UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.gray
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let BODLabel:UILabel = {
        let view = UILabel()
        view.text = "Birth of Date"
        view.textColor = UIColor.black
        view.font = UIFont.systemFont(ofSize: 20)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let BODTextField:UITextField = {
        let view = UITextField()
        view.borderStyle = .none
        view.placeholder = "Phone Number"
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    let BODLine:UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.gray
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let verifybutton:UIButton = {
        let view = UIButton()
        view.setTitle("Verify",for: .normal)
        view.setTitleColor(UIColor.rgb(red: 28, green: 87, blue: 154,alpha:1), for: .normal)
        view.layer.borderWidth = 1
        view.layer.borderColor = UIColor.rgb(red: 28, green: 87, blue: 154,alpha:1).cgColor
        view.layer.cornerRadius = 20
        view.addTarget(self, action: #selector(onClickDoneButton), for: .touchUpInside)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var cancelButton:UIButton = {
         let view = UIButton()
         view.setTitle("Cancel", for: UIControl.State.normal)
         view.titleLabel?.font = UIFont(name: "AirbnbCerealApp-Book",size: 17)
         view.setTitleColor(UIColor.white, for: UIControl.State.normal)
         view.translatesAutoresizingMaskIntoConstraints = false
        view.addTarget(self, action: #selector(onClickCancelButton), for: .touchUpInside)
         return view
         
     }()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUI()
       
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
              super.viewWillAppear(animated)

              // Hide the navigation bar on the this view controller
              self.navigationController?.setNavigationBarHidden(true, animated: animated)
              
          }
          
          override func viewWillDisappear(_ animated: Bool) {
                 super.viewWillAppear(animated)

                 // Hide the navigation bar on the this view controller
                 self.navigationController?.setNavigationBarHidden(false, animated: animated)
             }
    
    
    func setUI(){
        
        view.addSubview(scrollView)
        
        let ScrollviewCon = [
            scrollView.topAnchor.constraint(equalTo: view.topAnchor),
            scrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            scrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ]
        NSLayoutConstraint.activate(ScrollviewCon)
        
        
        //tabbar
        view.addSubview(tabView)
        
        tabView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        tabView.topAnchor.constraint(equalTo: view.topAnchor,constant: 0).isActive = true
        tabView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        tabView.heightAnchor.constraint(equalToConstant: 200).isActive = true
        
        
        tabView.addSubview(cancelButton)
        cancelButton.setConstraint(top: tabView.topAnchor, leading: nil, trailing: tabView.trailingAnchor, bottom: nil ,padding: .init(top: 40, left: 0, bottom: 0, right: 15))
        
    
        
        //Title
        tabView.addSubview(texttitle)
        texttitle.topAnchor.constraint(equalTo: view.topAnchor, constant: 150).isActive = true
        texttitle.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20).isActive = true
       // texttitle.bottomAnchor.constraint(equalTo: tabView.bottomAnchor, constant: 20).isActive = true
       // texttitle.trailingAnchor.constraint(equalTo: tabView.trailingAnchor).isActive = true
        
        
        //Firstnamelabel
       scrollView.addSubview(FNameLabel)
        let constraint = [
            FNameLabel.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 200),
        FNameLabel.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: 20)
        ]
        
        NSLayoutConstraint.activate(constraint)
        
        //first name textfield
        scrollView.addSubview(FNameTextField)
        let FnameTF = [FNameTextField.topAnchor.constraint(equalTo: FNameLabel.bottomAnchor, constant: 25),
        FNameTextField.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant:20),
        FNameTextField.widthAnchor.constraint(equalToConstant: 300),
        FNameTextField.heightAnchor.constraint(equalToConstant: 30)]
        
        NSLayoutConstraint.activate(FnameTF)
        
        
        scrollView.addSubview(FNameLine)
        let FnameL = [
            FNameLine.topAnchor.constraint(equalTo: FNameTextField.bottomAnchor),
            FNameLine.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            FNameLine.widthAnchor.constraint(equalTo: FNameTextField.widthAnchor),
            FNameLine.heightAnchor.constraint(equalToConstant: 1)
        
        ]
        NSLayoutConstraint.activate(FnameL)
        
        
       //Last Name
        
        scrollView.addSubview(LNameLabel)
               let LnameLb = [
               LNameLabel.topAnchor.constraint(equalTo: FNameLine.bottomAnchor, constant: 30),
               LNameLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20)
               ]
               
               NSLayoutConstraint.activate(LnameLb)
               
               //first name textfield
        scrollView.addSubview(LNameTextField)
               let LnameTF = [LNameTextField.topAnchor.constraint(equalTo: LNameLabel.bottomAnchor, constant: 25),
               LNameTextField.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant:20),
               LNameTextField.widthAnchor.constraint(equalToConstant: 300),
               LNameTextField.heightAnchor.constraint(equalToConstant: 30)]
               
        NSLayoutConstraint.activate(LnameTF)
               
               
               scrollView.addSubview(LNameLine)
               let LnameL = [
                LNameLine.topAnchor.constraint(equalTo: LNameTextField.bottomAnchor),
                   LNameLine.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
                   LNameLine.widthAnchor.constraint(equalTo: FNameTextField.widthAnchor),
                   LNameLine.heightAnchor.constraint(equalToConstant: 1)
               
               ]
               NSLayoutConstraint.activate(LnameL)
        
        
        //Phone Number
        
        
       scrollView.addSubview(PhoneNLabel)
                      let PhoneNLb = [
                      PhoneNLabel.topAnchor.constraint(equalTo: LNameLine.bottomAnchor, constant: 30),
                      PhoneNLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20)
                      ]
                      
                      NSLayoutConstraint.activate(PhoneNLb)
                      
                      //first name textfield
       scrollView.addSubview(PhoneNTextField)
                let PhoneNTF = [PhoneNTextField.topAnchor.constraint(equalTo: PhoneNLabel.bottomAnchor, constant: 25),
                    PhoneNTextField.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant:20),
                    PhoneNTextField.widthAnchor.constraint(equalToConstant: 300),
                    PhoneNTextField.heightAnchor.constraint(equalToConstant: 30)]
                      
               NSLayoutConstraint.activate(PhoneNTF)
                      
                      
      scrollView.addSubview(PhoneNLine)
                      let PhoneNL = [
                       PhoneNLine.topAnchor.constraint(equalTo: PhoneNTextField.bottomAnchor),
                          PhoneNLine.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
                          PhoneNLine.widthAnchor.constraint(equalTo: PhoneNTextField.widthAnchor),
                          PhoneNLine.heightAnchor.constraint(equalToConstant: 1)

                      ]
                      NSLayoutConstraint.activate(PhoneNL)
        
        
       scrollView.addSubview(BODLabel)
                             let BODLb = [
                             BODLabel.topAnchor.constraint(equalTo: PhoneNLine.bottomAnchor, constant: 30),
                             BODLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20)
                             ]
                             
                             NSLayoutConstraint.activate(BODLb)
                             
                             //first name textfield
      scrollView.addSubview(BODTextField)
                       let BODTF = [BODTextField.topAnchor.constraint(equalTo: BODLabel.bottomAnchor, constant: 25),
                           BODTextField.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant:20),
                           BODTextField.widthAnchor.constraint(equalToConstant: 300),
                           BODTextField.heightAnchor.constraint(equalToConstant: 30)]
                             
                      NSLayoutConstraint.activate(BODTF)
                             
                             
     scrollView.addSubview(BODLine)
                             let BODL = [
                              BODLine.topAnchor.constraint(equalTo: BODTextField.bottomAnchor),
                                 BODLine.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
                                 BODLine.widthAnchor.constraint(equalTo: BODTextField.widthAnchor),
                                 BODLine.heightAnchor.constraint(equalToConstant: 1)

                             ]
                             NSLayoutConstraint.activate(BODL)
        
    scrollView.addSubview(verifybutton)

        let verifyButtonCon = [
            verifybutton.topAnchor.constraint(equalTo: BODLine.bottomAnchor,constant:40),
            verifybutton.widthAnchor.constraint(equalToConstant: 200),
            verifybutton.heightAnchor.constraint(equalToConstant: 40),
            verifybutton.centerXAnchor.constraint(equalTo: view.centerXAnchor)
            
           
        ]

        NSLayoutConstraint.activate(verifyButtonCon)
    
        
        verifybutton.addTarget(self, action: #selector(onClickDoneButton), for: .touchUpInside)
        
               let datepicker = UIDatePicker()
               datepicker.datePickerMode = .date
               datepicker.addTarget(self, action: #selector(RegisterViewController.datechanged(datePicker:)), for: .valueChanged)
               BODTextField.inputView = datepicker
            
    }
    @objc func onClickDoneButton(){
        //view.endEditing(true)
        print("clicked already")
    }
    
    @objc func datechanged(datePicker: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyy"
        BODTextField.text = dateFormatter.string(from: datePicker.date)
        //view.endEditing(true)
    }
    @objc func onClickCancelButton(){
           //view.endEditing(true)
        self.dismiss(animated: true, completion: nil)
       }
        // it still be problem
        
        
        
//               let toolBar = UIToolbar()
//               toolBar.barStyle = .default
//               toolBar.isTranslucent = true
//               let space = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
//               let doneButton = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(onClickDoneButton))
//               toolBar.setItems([space, doneButton], animated: false)
//               toolBar.isUserInteractionEnabled = true
//               toolBar.sizeToFit()
//               BODTextField.inputAccessoryView = toolBar
        
                


        
        
        
        
        
        
        
        
      
       

        
        
//        let constraints = [
//            tabView.topAnchor.constraint(equalTo: view.topAnchor),
//            tabView.leftAnchor.constraint(equalTo: view.leftAnchor),
//            tabView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
//            tabView.rightAnchor.constraint(equalTo: view.rightAnchor)
//        ]
//
//        NSLayoutConstraint.activate(constraints)
       
        
    }
    
 
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */






