//
//  AllDeed.swift
//  DeedRequestApplication
//
//  Created by admin on 27/2/2563 BE.
//  Copyright © 2563 EarthBlooProduction. All rights reserved.
//

import UIKit

struct AllDeed :Decodable{
    var deed_id:Int!
    var customer:Customer?
    var employee:Employee?
    var deed_name:String!
    var image_path:[String]?
    var address:String!
    var nearby_landmark:String?
    var latitude:Double!
    var longitude:Double!
    var area:Double?
    var deed_number:String?
    var deed_type:Deed_type?
    var status_id:Int!
    var created_date:String!
    var updated_date:String?
    

}

struct Customer:Decodable{
    var customer_id:Int!
    var user_id:Int!
    var first_name:String!
    var last_name:String!
    var phone_number:String!
}

struct Employee:Decodable{
    var employee_id:Int!
    var user_id:Int!
    var first_name:String!
    var last_name:String!
}

struct Deed_type:Decodable {
    var deed_type_id:Int!
    var name:String!
}


