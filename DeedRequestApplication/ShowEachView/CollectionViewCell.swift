//
//  CollectionViewCell.swift
//  DeedRequestApplication
//
//  Created by admin on 16/3/2563 BE.
//  Copyright © 2563 EarthBlooProduction. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var UIImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        updateUI()
        // Initialization code
    }
    
    func setPhoto(image:UIImage){
        UIImageView.contentMode = .scaleAspectFit
       UIImageView.clipsToBounds = true
        let frame = CGRect(x: 0, y: 0, width: 200, height: 200)
        self.UIImageView.frame = frame
        self.UIImageView.image =  image
    }
    func updateUI(){
        UIImageView.layer.cornerRadius = 20
        UIImageView.layer.masksToBounds = true
    }
    


}
