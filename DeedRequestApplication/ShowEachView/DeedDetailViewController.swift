//
//  DeedDetailViewController.swift
//  DeedRequestApplication
//
//  Created by admin on 5/4/2563 BE.
//  Copyright © 2563 EarthBlooProduction. All rights reserved.
//

import UIKit
import ArcGIS
import SkeletonView

class DeedDetailViewController: UIViewController,showViewDelegate,AGSGeoViewTouchDelegate {
  
    
    
    private var graphicsOverlay:AGSGraphicsOverlay!
    var deed_id:String!
    private var map: AGSMap!
    var lat:Double!
    var long:Double!
    var presenter:ShowViewPresenter!
    var imageCollection:[UIImage] = []
    
    
    let loader:UIActivityIndicatorView = {
    let loader = UIActivityIndicatorView()
    loader.style = .large
    return loader
    }()
    
    let bgloader:UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
        
    }()
    
    let statusTAB:UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.red
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.shadowColor = UIColor.gray.cgColor
        view.layer.shadowOffset = CGSize(width: 0, height: 2)
        view.layer.shadowRadius = 6
        view.layer.shadowOpacity = 0.3
        return view
    }()
    
   
    
    let scrollView:UIScrollView = {
           let view = UIScrollView()
           view.contentSize.height = 1550
        view.backgroundColor = UIColor.white
           view.translatesAutoresizingMaskIntoConstraints = false
           return view
       }()
    
    let pageController:UIPageControl = {
        let view = UIPageControl()
        view.currentPage = 0
        view.currentPageIndicatorTintColor = .lightGray
        view.translatesAutoresizingMaskIntoConstraints = false
        view.pageIndicatorTintColor = UIColor.black
        return view
    }()
    
    let backButton:UIButton = {
        let view = UIButton()
        view.setImage(UIImage(named: "back-2"), for: .normal)
        
        view.addTarget(self, action: #selector(back), for: .touchUpInside)
        return view
    }()
    


    fileprivate let ImageCollection:UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        
        layout.minimumLineSpacing = 0
        //layout.minimumInteritemSpacing = 0
        layout.scrollDirection = .horizontal
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        let bgimage = UIImageView()
        bgimage.image = UIImage(named:"no-photo")
        bgimage.contentMode = .center
        layout.collectionView?.backgroundView = bgimage
        
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.register(ImageCollectionCell.self, forCellWithReuseIdentifier: "Cell")
        cv.isPagingEnabled = true
        return cv
    }()
    
    
    let TitleLabel:UILabel = {
        let view = UILabel()
        let string = "Srinakharinwirot University "
        view.sizeToFit()
        view.numberOfLines = 0
        view.lineBreakMode = .byWordWrapping
        view.text = string.uppercased()
        view.textColor = UIColor.black
        view.font = UIFont(name: "Prompt-Medium", size: 30)
        
        
        //view.font = UIFont.systemFont(ofSize: 27)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
        
    }()
    
    let AddressLabel:UILabel = {
        let view = UILabel()
        let string = "Office of the President, Srinakharinwirot University 114 Sukhumvit 23, Wattana District, Bangkok 10110, THAILAND"
        view.sizeToFit()
        
        view.numberOfLines = 0
        view.lineBreakMode = .byWordWrapping
        view.text = string
        view.textColor = UIColor.gray
        view.font = UIFont(name: "AirbnbCerealApp-Book", size: 17)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
        
    }()
    
    let StatusLabel:UILabel = {
        let view = UILabel()
        view.text = "Status"
        view.textAlignment = .center
        view.layer.borderWidth = 0.7
        view.layer.borderColor = UIColor.gray.cgColor
        view.layer.cornerRadius = 10
        view.translatesAutoresizingMaskIntoConstraints = false
        view.font = UIFont(name: "Prompt-Light",size: 17)
        return view
    }()
    
    let tabView:UIView = {
        let view = UIView()
        view.layer.shadowColor = UIColor.gray.cgColor
        view.layer.shadowOffset = CGSize(width: 2.0, height: 2.0);
        view.layer.shadowRadius = 3.0
        view.layer.shadowOpacity = 0.8
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        return view
    }()
    
    let CancleButton:UIButton = {
        let view = UIButton()
        view.layer.borderWidth = 1
        view.layer.borderColor = UIColor.red.cgColor
        view.layer.cornerRadius = 20
        view.setTitle("Cancel", for: UIControl.State.normal)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.titleLabel?.font = UIFont(name: "Prompt-Light",size: 18)
        view.setTitleColor(.red, for: UIControl.State.normal)
       
        view.addTarget(self, action: #selector(CancelRequest), for: .touchUpInside)
        return view
    }()
    
    
    
    let AreaLabel:UILabel = {
    let view = UILabel()
    view.text = "In Progress"
    view.textColor = UIColor.black
    view.font = UIFont(name: "Prompt-Light",size: 23)
    view.translatesAutoresizingMaskIntoConstraints = false
    return view
    }()
    
    let divider1:UIView = {
           let view = UIView()
           view.backgroundColor = UIColor.gray
           view.translatesAutoresizingMaskIntoConstraints = false
           return(view)
       }()
    
    let DetailLabel:UILabel = {
        let view = UILabel()
        view.text = "DETAIL"
        view.textColor = UIColor.black
        view.font = UIFont.systemFont(ofSize: 25)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.font = UIFont(name: "AirbnbCerealApp-Book",size: 23)
        return view
    }()
    
    let iconDeedNumber:UIImageView = {
        let view = UIImageView()
        view.image = UIImage(named: "DeedNumber")
        //view.backgroundColor = UIColor.black
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let DeedNumberTitle:UILabel = {
        let view = UILabel()
        view.text = "Deed Number"
        view.textColor = UIColor.black
        view.font = UIFont(name: "AirbnbCerealApp-Book",size: 20)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let DeedNumberLabel:UILabel = {
        let view = UILabel()
        view.text = "In Progress"
        view.textColor = UIColor.gray
        view.font = UIFont(name: "Prompt-Light",size: 20)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let iconTypeDeed:UIImageView = {
        let view = UIImageView()
        view.image = UIImage(named: "Land")
        //view.backgroundColor = UIColor.black
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let TypeDeedTitle:UILabel = {
        let view = UILabel()
        view.text = "Type of Deed"
        view.textColor = UIColor.black
        view.font = UIFont(name: "AirbnbCerealApp-Book",size: 20)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let TypeDeedLabel:UILabel = {
        let view = UILabel()
        view.text = "In Progress"
        view.textColor = UIColor.gray
         view.font = UIFont(name: "Prompt-Light",size: 20)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    
    let iconDate:UIImageView = {
        let view = UIImageView()
        view.image = UIImage(named: "Date")
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let DateTitle:UILabel = {
        let view = UILabel()
        view.text = "Created Date"
        view.textColor = UIColor.black
        view.font = UIFont(name: "AirbnbCerealApp-Book",size: 20)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let DateLabel:UILabel = {
        let view = UILabel()
        view.text = "12 january 2560"
        view.textColor = UIColor.gray
         view.font = UIFont(name: "Prompt-Light",size: 20)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let iconEmploy:UIImageView = {
           let view = UIImageView()
           view.image = UIImage(named: "Employee")
           view.translatesAutoresizingMaskIntoConstraints = false
           return view
       }()
       
       let EmployTitle:UILabel = {
           let view = UILabel()
           view.text = "Employee Name"
           view.textColor = UIColor.black
          view.font = UIFont(name: "AirbnbCerealApp-Book",size: 20)
           view.translatesAutoresizingMaskIntoConstraints = false
           return view
       }()
       
       let EmployLabel:UILabel = {
           let view = UILabel()
           view.text = "In Progress"
           view.textColor = UIColor.gray
            view.font = UIFont(name: "Prompt-Light",size: 20)
           view.translatesAutoresizingMaskIntoConstraints = false
           return view
       }()
    
    
    let iconLandmark:UIImageView = {
        let view = UIImageView()
        view.image = UIImage(named: "landmark")
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let landmarkTitle:UILabel = {
        let view = UILabel()
        view.text = "Nearby Landmark"
        view.textColor = UIColor.black
        view.font = UIFont(name: "AirbnbCerealApp-Book",size: 20)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let landmarkLabel:UILabel = {
        let view = UILabel()
        view.text = " - "
        view.textColor = UIColor.gray
         view.font = UIFont(name: "Prompt-Light",size: 20)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
        
      let divider2:UIView = {
          let view = UIView()
          view.backgroundColor = UIColor.gray
          view.translatesAutoresizingMaskIntoConstraints = false
          return(view)
      }()
    
    let mapView:AGSMapView = {
        let view = AGSMapView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
        
    }()
    let pinImage:UIImageView = {
        let view = UIImageView()
        view.image = UIImage(named: "MAP-LOGO")
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let LocationLabel:UILabel = {
           let view = UILabel()
           view.text = "LOCATION"
           view.textColor = UIColor.black
           view.font = UIFont(name: "AirbnbCerealApp-Book",size: 23)
           view.translatesAutoresizingMaskIntoConstraints = false
           return view
       }()
    
    
    var status1:String!
    var status2:String!
    var status3:String!
    var status4:String!
    //---------------------------------------------------------------------------------//
    //---------------------------------------------------------------------------------//
    //---------------------------------------------------------------------------------//

   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUI()
        presenter = ShowViewPresenter()
        presenter.delegate = self
        mapView.touchDelegate = self
        self.loader.hidesWhenStopped = true
        self.loader.center = self.view.center
        self.view.addSubview(self.loader)
        setConfig()
        setSkeleton()
        //self.loader.startAnimating()

    }
    
    func mapValue(i:Int) -> String {
        let statusCon = appConfig.shared.con.resources.status!
        let st1 = statusCon.filter { status in status.status_id == i }.map { status in status.name }
        guard let st = st1[0] else{
            return ""
        }
        return st
    }
    
    func setConfig(){
           
           status1 = appConfig.shared.con.resources.status[0].name
           status2 = appConfig.shared.con.resources.status[1].name
           status3 = appConfig.shared.con.resources.status[2].name
           status4 = appConfig.shared.con.resources.status[3].name
       }
    
    func setSkeleton(){
         
         scrollView.isSkeletonable = true
         ImageCollection.isSkeletonable = true
         TitleLabel.isSkeletonable = true
         StatusLabel.isSkeletonable = true
         AddressLabel.isSkeletonable = true
         DetailLabel.isSkeletonable = true
         iconDeedNumber.isSkeletonable = true
         DeedNumberLabel.isSkeletonable = true
         TypeDeedTitle.isSkeletonable = true
         iconTypeDeed.isSkeletonable = true
         statusTAB.isSkeletonable = true
         AreaLabel.isSkeletonable = true
         CancleButton.isSkeletonable = true
         DeedNumberTitle.isSkeletonable = true
         ImageCollection.isSkeletonable = true
         TitleLabel.linesCornerRadius = 10
         StatusLabel.linesCornerRadius = 10
        AddressLabel.linesCornerRadius = 10
         DetailLabel.linesCornerRadius = 10

        DeedNumberLabel.linesCornerRadius = 10
        TypeDeedTitle.linesCornerRadius = 10

        AreaLabel.linesCornerRadius = 10
   
        DeedNumberTitle.linesCornerRadius = 10
      
         tabView.showAnimatedSkeleton()
         scrollView.showAnimatedSkeleton()
        
       
       
        
        
        
    }
  
    
    
    
 
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        let x = targetContentOffset.pointee.x
        pageController.currentPage = Int(x / view.frame.width)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
       var x = scrollView.contentOffset.y / 130
    print(scrollView.contentOffset.y / 130)
        
        if x > 1 {
            x = 1
           statusTAB.backgroundColor = UIColor.init(red: 250/250, green: 250/250, blue: 250/250, alpha: 1)
           statusTAB.alpha = 1
       
        }else{
            statusTAB.alpha = 0
        }
        
        
    }
    @objc func back(_ sender: UIButton){
        backButton.pulse()
        
        self.dismiss(animated: true, completion: nil)
    }
    
    func setUI(){
        scrollView.delegate = self
//        statusBar.alpha = 0
        

      // self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(title: "LEFT", style: .done, target: self, action: nil)

        view.addSubview(scrollView)
               let ScrollviewCon = [
                   scrollView.topAnchor.constraint(equalTo: view.topAnchor),
                   scrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
                   scrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
                   scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
               ]
        NSLayoutConstraint.activate(ScrollviewCon)
       
       
       
        //imageCollection
        scrollView.addSubview(ImageCollection)
        ImageCollection.backgroundColor = .white
        ImageCollection.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: -47).isActive = true
        ImageCollection.leadingAnchor.constraint(equalTo: view.leadingAnchor,constant: 0).isActive = true
        ImageCollection.trailingAnchor.constraint(equalTo: view.trailingAnchor,constant: 0).isActive = true
        ImageCollection.heightAnchor.constraint(equalToConstant: view.frame.height * 0.35).isActive = true
        ImageCollection.delegate = self
        ImageCollection.dataSource = self
        
        
        //pageController
        scrollView.addSubview(pageController)
        let PageControllerCon = [
           pageController.centerXAnchor.constraint(equalTo: view.centerXAnchor),
          
           pageController.bottomAnchor.constraint(equalTo: ImageCollection.bottomAnchor, constant: -10)
]
        NSLayoutConstraint.activate(PageControllerCon)
        
        
    
        //TitleLabel
        scrollView.addSubview(TitleLabel)
        TitleLabel.setConstraint(top: ImageCollection.bottomAnchor, leading: view.leadingAnchor, trailing: view.trailingAnchor, bottom: nil, padding: .init(top: 15, left: 25, bottom: 0, right: 25))
        
        //AddressLabel
        scrollView.addSubview(AddressLabel)
        AddressLabel.setConstraint(top: TitleLabel.bottomAnchor, leading: view.leadingAnchor, trailing: view.trailingAnchor, bottom: nil, padding: .init(top: 25, left: 25, bottom: 0, right: 180))
        
        
        //statusLabel
        scrollView.addSubview(StatusLabel)
        StatusLabel.setConstraint(top: TitleLabel.bottomAnchor, leading: nil, trailing: view.trailingAnchor, bottom: nil, padding: .init(top: 25, left: 0, bottom: 0, right: 30),size: .init(width: 120, height: 30))
                   

        
        
        //divider1
        
        scrollView.addSubview(divider1)
        
        divider1.setConstraint(top: AddressLabel.bottomAnchor, leading: nil, trailing: nil, bottom: nil, padding: .init(top: 25, left: 0, bottom: 0, right: 0), size: .init(width: 360, height: 0.5))

        divider1.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
       
        
        
        //Detail
        scrollView.addSubview(DetailLabel)
        
        DetailLabel.setConstraint(top: divider1.bottomAnchor, leading: view.leadingAnchor, trailing: view.trailingAnchor, bottom: nil, padding: .init(top: 24, left: 25, bottom: 0, right: 25))
        
      
        
  
        
        
        //Deed Number
        scrollView.addSubview(iconDeedNumber)
        
        iconDeedNumber.setConstraint(top: DetailLabel.bottomAnchor, leading: view.leadingAnchor, trailing: nil, bottom: nil, padding: .init(top: 25, left: 45, bottom: 0, right: 0), size: .init(width: 26, height: 26))

        
        //DeedNumberTitle
        scrollView.addSubview(DeedNumberTitle)
        DeedNumberTitle.setConstraint(top: DetailLabel.bottomAnchor, leading: iconDeedNumber.trailingAnchor, trailing: view.trailingAnchor, bottom: nil, padding: .init(top: 25, left: 10, bottom: 0, right: 25))
//        DeedNumberTitle.topAnchor.constraint(equalTo: DetailLabel.bottomAnchor,constant: 25).isActive = true
//        DeedNumberTitle.leadingAnchor.constraint(equalTo: iconDeedNumber.trailingAnchor,constant: 10).isActive = true
//        DeedNumberTitle.trailingAnchor.constraint(equalTo: view.trailingAnchor,constant: 25).isActive = true
        //DeedNumberLabel
        scrollView.addSubview(DeedNumberLabel)
        DeedNumberLabel.setConstraint(top: DeedNumberTitle.bottomAnchor, leading: view.leadingAnchor, trailing: view.trailingAnchor, bottom: nil, padding: .init(top: 15, left: 80, bottom: 0, right: 25))
//        DeedNumberLabel.topAnchor.constraint(equalTo: DeedNumberTitle.bottomAnchor,constant: 15).isActive = true
//        DeedNumberLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor,constant: 80).isActive = true
//        DeedNumberLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor,constant: 25).isActive = true
        
        
        scrollView.addSubview(iconTypeDeed)
        iconTypeDeed.setConstraint(top: iconDeedNumber.bottomAnchor, leading: view.leadingAnchor, trailing: nil, bottom: nil, padding: .init(top: 62, left: 40, bottom: 0, right: 0), size: .init(width: 26, height: 26))
        

        
        //DeedNumberTitle
        scrollView.addSubview(TypeDeedTitle)
        
        TypeDeedTitle.setConstraint(top: DeedNumberLabel.bottomAnchor, leading: iconTypeDeed.trailingAnchor, trailing: view.trailingAnchor, bottom: nil, padding: .init(top: 20, left: 10, bottom: 25, right: 0))
      
        
        scrollView.addSubview(TypeDeedLabel)
        
        TypeDeedLabel.setConstraint(top: TypeDeedTitle.bottomAnchor, leading: view.leadingAnchor, trailing: view.trailingAnchor, bottom: nil, padding: .init(top: 15, left: 80, bottom: 0, right: 25))
      
        scrollView.addSubview(iconDate)
        
        iconDate.setConstraint(top: iconTypeDeed.bottomAnchor, leading: view.leadingAnchor, trailing: nil, bottom: nil, padding: .init(top: 80, left: 40, bottom: 0, right: 0), size: .init(width: 26, height: 26))
               
           
               
               //DeedNumberTitle
        scrollView.addSubview(DateTitle)
        
        DateTitle.setConstraint(top: TypeDeedLabel.bottomAnchor, leading: iconDate.trailingAnchor, trailing: view.trailingAnchor, bottom: nil, padding: .init(top: 30, left: 10, bottom: 0, right: 25))
             
               //DeedNumberLabel
               scrollView.addSubview(DateLabel)
        
        DateLabel.setConstraint(top: DateTitle.bottomAnchor, leading: view.leadingAnchor, trailing: view.trailingAnchor, bottom: nil, padding: .init(top: 15, left: 80, bottom: 0, right: 25))
              


        scrollView.addSubview(iconEmploy)
        
        iconEmploy.setConstraint(top: iconDate.bottomAnchor, leading: view.leadingAnchor, trailing: nil, bottom: nil, padding: .init(top: 72, left: 40, bottom: 0, right: 0), size: .init(width: 29, height: 29))
                      
           
                      
                      //DeedNumberTitle
                      scrollView.addSubview(EmployTitle)
        EmployTitle.setConstraint(top: DateLabel.bottomAnchor, leading: iconDate.trailingAnchor, trailing: view.trailingAnchor, bottom: nil, padding: .init(top: 30, left: 10, bottom: 0, right: 25))
     
                      //DeedNumberLabel
                      scrollView.addSubview(EmployLabel)
        EmployLabel.setConstraint(top: EmployTitle.bottomAnchor, leading: view.leadingAnchor, trailing: view.trailingAnchor, bottom: nil, padding: .init(top: 15, left: 80, bottom: 0, right: 25))
                  
        
        
        scrollView.addSubview(iconLandmark)
        iconLandmark.setConstraint(top: iconEmploy.bottomAnchor, leading: view.leadingAnchor, trailing: nil, bottom: nil, padding: .init(top: 82, left: 40, bottom: 0, right: 0), size: .init(width: 29, height: 29))
                      
          
                      
                      //DeedNumberTitle
                      scrollView.addSubview(landmarkTitle)
        
        landmarkTitle.setConstraint(top: EmployLabel.bottomAnchor, leading: iconEmploy.trailingAnchor, trailing: view.trailingAnchor, bottom: nil, padding: .init(top: 35, left: 10, bottom: 0, right: 25))
           
                      //DeedNumberLabel
                      scrollView.addSubview(landmarkLabel)
        landmarkLabel.setConstraint(top: landmarkTitle.bottomAnchor, leading: view.leadingAnchor, trailing: view.trailingAnchor, bottom: nil, padding: .init(top: 15, left: 80, bottom: 0, right: 25))
    
        
        
        //divider1
        
        scrollView.addSubview(divider2)
        divider2.setConstraint(top: landmarkLabel.bottomAnchor, leading: nil, trailing: nil, bottom: nil, padding: .init(top: 35, left: 0, bottom: 0, right: 0), size: .init(width: 360, height: 0.5))
        divider2.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        
        
        
        scrollView.addSubview(LocationLabel)
        LocationLabel.setConstraint(top: divider2.bottomAnchor, leading: view.leadingAnchor, trailing: view.trailingAnchor, bottom: nil, padding: .init(top: 25, left: 25, bottom: 0, right: 25))
        
   
        
        
        
        //mapview
        
        scrollView.addSubview(mapView)
        mapView.setConstraint(top: LocationLabel.bottomAnchor, leading: nil, trailing: nil, bottom: nil, padding: .init(top: 45, left: 0, bottom: 0, right: 0),size: .init(width: 350, height: 350))
        mapView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
    
        
        
        

        //tabbar
        view.addSubview(tabView)
        
        tabView.setConstraint(top: nil, leading: view.leadingAnchor, trailing: view.trailingAnchor, bottom: view.bottomAnchor, padding: .init(top: 0, left: 0, bottom: 0, right: 0), size: .init(width: 0, height: 100))
        
   
        
        
        
        
        //Cancle BUtton
        
        tabView.addSubview(CancleButton)
        CancleButton.setConstraint(top: nil, leading: nil, trailing: tabView.trailingAnchor, bottom: tabView.bottomAnchor, padding: .init(top: 0, left: 0, bottom: 30, right: 30), size: .init(width: 120, height: 45))
      
        
        //AreaLabel
        tabView.addSubview(AreaLabel)
        AreaLabel.setConstraint(top: nil, leading: tabView.leadingAnchor, trailing: nil, bottom: tabView.bottomAnchor, padding: .init(top: 0, left: 30, bottom: 45, right: 0), size: .init(width: 200, height: 0))
        
       
        
        view.addSubview(statusTAB)
        
        statusTAB.setConstraint(top: view.topAnchor, leading: view.leadingAnchor, trailing: view.trailingAnchor, bottom: nil, padding: .init(top: 0, left: 0, bottom: 0, right: 0), size: .init(width: 0, height: 95))

        
        view.addSubview(backButton)
        
        backButton.setConstraint(top: view.topAnchor, leading: view.leadingAnchor, trailing: view.trailingAnchor, bottom: nil, padding: .init(top: 50, left: 15, bottom: 0, right: 360),size: .init(width: 0, height: 40))
        

//        view.addSubview(bgloader)
//               let bgCon = [
//                   bgloader.topAnchor.constraint(equalTo: view.topAnchor),
//                   bgloader.leadingAnchor.constraint(equalTo: view.leadingAnchor),
//                   bgloader.trailingAnchor.constraint(equalTo: view.trailingAnchor),
//                   bgloader.bottomAnchor.constraint(equalTo: view.bottomAnchor)
//               ]
//               NSLayoutConstraint.activate(bgCon)
   
        }
    func setImage(imageUI: [UIImage]) {
     imageCollection = imageUI
        
    }
    
    
    func setupMap(){
          // print("showView = \(lat) and \(long)")
        map = AGSMap(basemapType: .openStreetMap, latitude: lat, longitude: long, levelOfDetail: 18)
           mapView.map = map
           mapView.layer.cornerRadius = 13
       }
    func geoView(_ geoView: AGSGeoView, didTapAtScreenPoint screenPoint: CGPoint, mapPoint: AGSPoint) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MapDetailViewController") as! MapDetailViewController
        vc.lat = lat
        vc.long = long
        
         vc.modalPresentationStyle = .fullScreen
        vc.modalTransitionStyle = .coverVertical
        self.present(vc, animated: true, completion: nil)
        
    }
    
    
    private func createGraphics() {
        graphicsOverlay = AGSGraphicsOverlay()
        self.mapView.graphicsOverlays.add(graphicsOverlay)
        let markerImage = UIImage(named: "MAP-LOGO")!
        let symbol = AGSPictureMarkerSymbol(image: markerImage)
        //symbol.offsetX = markerImage.size.height
        symbol.offsetY = markerImage.size.height/2
       // symbol.leaderOffsetY = markerImage.size.height / 2
       // symbol.offsetX = markerImage.size.height/2
        let pinSymbol = AGSSimpleMarkerSymbol(style: .circle, color: .blue, size: 5)
        let point = AGSPoint(x:long, y: lat, spatialReference: AGSSpatialReference.wgs84())
            
        let pointGraphic = AGSGraphic(geometry: point, symbol: pinSymbol, attributes: nil)
        let imgGraphic = AGSGraphic(geometry: point, symbol: symbol, attributes: nil)
        graphicsOverlay.graphics.add(pointGraphic)
        graphicsOverlay.graphics.add(imgGraphic)
        }
    
    @objc func CancelRequest(_ sender: UIButton){
        CancleButton.pulse()
        
        
        
        let alert = UIAlertController(title: "Delete", message: "Do you want to cancel this?", preferredStyle: .alert)
               alert.addAction(UIAlertAction(title:"No",style: .cancel,handler: nil))
               alert.addAction(UIAlertAction(title:"Yes",style: .default,handler: {
                   (action:UIAlertAction) in
                
                if self.StatusLabel.text != self.status4 {
                    if self.StatusLabel.text != self.status3 {
                        self.presenter.cancleOrder(x: self.deed_id)
                        self.dismiss(animated: true, completion: nil)
                    
                   
                    }else {
                        let alert = UIAlertController(title: "Alert", message: "The request have been \(self.status3!)", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title:"Ok",style: .default,handler:nil))
                        self.present(alert,animated:true)
                        
                    }
                    
                }else{
                    let alert = UIAlertController(title: "Alert", message: "The request have been \(self.status4!)", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title:"Ok",style: .default,handler:nil))
                    self.present(alert,animated:true)
                    
                    
                }
                
                
                }))
                       
        present(alert,animated:true)
    }
    
    
    func updateTable() {
        
       
        
        loader.stopAnimating()
        self.ImageCollection.reloadData()
        bgloader.removeFromSuperview()
    }
    
    
    
    func setData(deedview: AllDeed) {
       view.hideSkeleton()
        if let deed_number = deedview.deed_number {
        DeedNumberLabel.text = deed_number
        }
        
        AddressLabel.text = deedview.address
        
        if let area = deedview.area{
            let  area_s = String(area)
            AreaLabel.text = "\(area_s) SQM"
        }
        
        if let deed = deedview.deed_type?.name{
            TypeDeedLabel.text = deed
        }
        
        if let landmark = deedview.nearby_landmark{
                  landmarkLabel.text = landmark
              }
        
        TitleLabel.text = deedview.deed_name.uppercased()
        StatusLabel.text = mapValue(i:deedview.status_id)
        
        // set Date
        if let Deeddate =  deedview.created_date {
        var dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "E, dd MMM yyyy HH:mm:ss z"// yyyy-MM-dd"
        let date = dateFormatter.date(from: Deeddate)
        dateFormatter.dateFormat = "EEEE dd MMMM yyyy"
        let dataString = dateFormatter.string(from: date!)
        DateLabel.text = dataString
        }
        

        if let empName = deedview.employee?.first_name { EmployLabel.text = empName }
        lat = deedview.latitude
        long = deedview.longitude
       
        
        
        setupMap()
        createGraphics()
     
       }
    
    
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        
         presenter.showData(deed_id:deed_id)
        
    }
    
   
    
    
        
        
    }
    

     
       
    
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */



extension DeedDetailViewController:UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,SkeletonCollectionViewDataSource {
    func collectionSkeletonView(_ skeletonView: UICollectionView, cellIdentifierForItemAt indexPath: IndexPath) -> ReusableCellIdentifier {
        return "Cell"
        
    }
    
  
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: ImageCollection.frame.width, height: ImageCollection.bounds.height )
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageCollection.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = ImageCollection.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! ImageCollectionCell
        pageController.numberOfPages = imageCollection.count
        
        cell.setImage(image:imageCollection[indexPath.row])
        
        //cell.backgroundColor = indexPath.item % 2 == 0 ? .red : .green
        return cell
    }
    
    
    
    
}
