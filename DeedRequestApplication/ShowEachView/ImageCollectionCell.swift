//
//  ImageCollectionCell.swift
//  DeedRequestApplication
//
//  Created by admin on 5/4/2563 BE.
//  Copyright © 2563 EarthBlooProduction. All rights reserved.
//

import UIKit

class ImageCollectionCell:UICollectionViewCell{
    
    fileprivate let imageview:UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.contentMode = .scaleAspectFill
        view.clipsToBounds = true
       
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame:frame)
        
        contentView.addSubview(imageview)
        
        imageview.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 0).isActive = true
        imageview.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 0).isActive = true
        imageview.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: 0).isActive = true
        imageview.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: 0).isActive = true
        imageview.isSkeletonable = true
        imageview.showSkeleton()
       
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setImage(image:UIImage){
        imageview.hideSkeleton()
        imageview.image = image
    }
    
}
