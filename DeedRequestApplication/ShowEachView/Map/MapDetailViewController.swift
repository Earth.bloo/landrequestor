//
//  MapDetailViewController.swift
//  DeedRequestApplication
//
//  Created by admin on 17/4/2563 BE.
//  Copyright © 2563 EarthBlooProduction. All rights reserved.
//




import UIKit
import ArcGIS

    class MapDetailViewController: UIViewController {
        @IBOutlet weak var mapView: AGSMapView!
        @IBOutlet weak var backButton: UIButton!
        private var graphicsOverlay:AGSGraphicsOverlay!
        
        var lat = 13.00
        var long = 100.00
        
        let curlocatButton:UIButton = {
            let view = UIButton()
            //view.backgroundColor = UIColor.white
            let cursor = UIImage(named: "current_btn")
            view.setBackgroundImage(cursor, for: .normal)
            view.translatesAutoresizingMaskIntoConstraints = false
            view.addTarget(self, action: #selector(setupLocationDisplay), for: .touchUpInside)
            
            return view
        }()

        override func viewDidLoad() {
            super.viewDidLoad()
            setUI()
           
            // Do any additional setup after loading the view.
        }
     
        
        func setUI(){
            
            setMap()
           backButton.layer.shadowColor = UIColor.gray.cgColor
           backButton.layer.shadowOffset = CGSize(width: 0, height: 2)
           backButton.layer.shadowRadius = 6
           backButton.layer.shadowOpacity = 0.3
            backButton.layer.cornerRadius = 60
            mapView.addSubview(curlocatButton)
            curlocatButton.setConstraint(top: nil, leading: mapView.leadingAnchor, trailing: nil, bottom: mapView.bottomAnchor, padding: .init(top: 0, left: 15, bottom: 65, right: 0), size: .init(width: 50, height: 50))
        }
        
        func setMap(){
           let map = AGSMap(basemapType: .openStreetMap, latitude: lat, longitude: long, levelOfDetail: 18)
            mapView.map = map
            createGraphics()
        }
        
        private func createGraphics() {
              graphicsOverlay = AGSGraphicsOverlay()
              self.mapView.graphicsOverlays.add(graphicsOverlay)
              let markerImage = UIImage(named: "MAP-LOGO")!
              let symbol = AGSPictureMarkerSymbol(image: markerImage)
              symbol.offsetY = markerImage.size.height/2
              let pinSymbol = AGSSimpleMarkerSymbol(style: .circle, color: .blue, size: 5)
              let point = AGSPoint(x:long, y: lat, spatialReference: AGSSpatialReference.wgs84())
              let imgGraphic = AGSGraphic(geometry: point, symbol: symbol, attributes: nil)
             
              graphicsOverlay.graphics.add(imgGraphic)
              }
        
        @objc func setupLocationDisplay() {
            mapView.locationDisplay.autoPanMode = AGSLocationDisplayAutoPanMode.recenter
            mapView.locationDisplay.start { [weak self] (error:Error?) -> Void in
                if let error = error {
                    self?.showAlert(withStatus: error.localizedDescription)
                }
               // self?.mapView.locationDisplay.stop()
                
            }
            
        }
    private func showAlert(withStatus: String) {
               let alertController = UIAlertController(title: "Alert", message:
                   withStatus, preferredStyle: UIAlertController.Style.alert)
               alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertAction.Style.default,handler: nil))
               present(alertController, animated: true, completion: nil)
           }
        @IBAction func BackButton(_ sender: Any) {
            
            self.dismiss(animated: true, completion: nil)
        }
}
