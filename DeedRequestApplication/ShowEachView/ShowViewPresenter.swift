//
//  ShowViewPresenter.swift
//  DeedRequestApplication
//
//  Created by admin on 27/2/2563 BE.
//  Copyright © 2563 EarthBlooProduction. All rights reserved.
//

import UIKit
import TLPhotoPicker




protocol showViewDelegate {
    func setData(deedview:AllDeed)
    func setImage(imageUI:[UIImage])
    func updateTable()
}

class ShowViewPresenter{
    var delegate:showViewDelegate!
    var alldeed:AllDeed!
   
    
    
    func showData(deed_id:String){
        callingAPI(deed_id:deed_id)
    }
    
    func callingAPI(deed_id:String) {
        var semaphore = DispatchSemaphore (value: 0)

        var request = URLRequest(url: URL(string: "http://land-surveyor-dev.eba-atp2ae9g.ap-southeast-1.elasticbeanstalk.com/api/v1/deeds/\(deed_id)")!,timeoutInterval: Double.infinity)
        request.httpMethod = "GET"

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
          guard let data = data else {
            print(String(describing: error))
            return
          }
            do{
                  let decoder = JSONDecoder()
                 self.alldeed =  try decoder.decode(AllDeed.self, from: data)
          
              }catch{
                  print(String(describing: error))
                  
              }
            semaphore.signal()
          }

        task.resume()
        semaphore.wait()
        delegate.setData(deedview: self.alldeed)
        
        guard let images = self.alldeed.image_path else {
            delegate.updateTable()
            return
        }
        let UIImage = convertURLtoImage(image_path:images)
        delegate.setImage(imageUI: UIImage)
    }
    
    
   
    
    func cancleOrder(x:String){
        print("delete(deed_id)==== \(x)")
        var semaphore = DispatchSemaphore (value: 0)

        let parameters = "{\n\t\"status_id\": 5\n}"
        let postData = parameters.data(using: .utf8)

        var request = URLRequest(url: URL(string: "http://land-surveyor-dev.eba-atp2ae9g.ap-southeast-1.elasticbeanstalk.com/api/v1/deeds/\(x)")!,timeoutInterval: Double.infinity)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
      

        request.httpMethod = "PATCH"
        request.httpBody = postData

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
          guard let data = data else {
            print(String(describing: error))
            return
          }
          print(String(data: data, encoding: .utf8)!)
          semaphore.signal()
        }

        task.resume()
        semaphore.wait()
        
}
    
    
    
    
    
    
    
    func convertURLtoImage(image_path:[String]) -> [UIImage]{
        var imageUI:[UIImage] = []
        for (index,value) in image_path.enumerated() {
            print(value)
            imageUI.append(imageAPI(image_path: value))
            delegate.updateTable()
}
        
        return imageUI
    }
    
 
    func imageAPI(image_path:String)-> UIImage{
        
        var imageUI:UIImage!

        var semaphore = DispatchSemaphore (value: 0)
    
        var request = URLRequest(url: URL(string: "http://land-surveyor-dev.eba-atp2ae9g.ap-southeast-1.elasticbeanstalk.com\(image_path)")!,timeoutInterval: Double.infinity)
        request.httpMethod = "GET"

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
          guard let data = data else {
            print(String(describing: error))
            return
          }
        imageUI = UIImage(data: data)
            
        // print(String(data: data, encoding: .utf8)!)
          semaphore.signal()
        }

        task.resume()
        semaphore.wait()
        
        
        return imageUI
        
       
        
        
    }
    
    
    
}
