//
//  File.swift
//  DeedRequestApplication
//
//  Created by admin on 17/2/2563 BE.
//  Copyright © 2563 EarthBlooProduction. All rights reserved.
//

import UIKit

class SourceCodeBarButtonItem: UIBarButtonItem {
    var filenames = [String]()
    weak var navController: UINavigationController?
    var readmeURL: URL?
    
    override init() {
        super.init()
        self.image = UIImage(named: "InfoIcon")
        self.target = self
        self.action = #selector(SourceCodeBarButtonItem.showSegmentedVC)
    }

    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc
    func showSegmentedVC() {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let controller = storyboard.instantiateViewController(withIdentifier: "SegmentedViewController") as! SegmentedViewController
        controller.filenames = filenames
        controller.readmeURL = readmeURL
        self.navController?.show(controller, sender: self)
    }
}
