//
//  TryViewController.swift
//  DeedRequestApplication
//
//  Created by admin on 16/2/2563 BE.
//  Copyright © 2563 EarthBlooProduction. All rights reserved.
//

import UIKit
import ArcGIS

class TryViewController: UIViewController, AGSGeoViewTouchDelegate {
    @IBOutlet private weak var mapView: AGSMapView!
    
    @IBOutlet weak var label: UILabel!
    private var map: AGSMap!
    private let graphicsOverlay = AGSGraphicsOverlay()
    
    var point = AGSPoint(x: 0, y: 0, spatialReference: .webMercator()) {
    didSet {
        // display a graphic at the point
        displayGraphicAtPoint(point)
        // populate the coordinate strings for the new point
       // tableViewController?.point = point
    }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //add the source code button item to the right of navigation bar
       
        
        //initializer map with basemap
        let map = AGSMap(basemap: .topographic())
        
        //assign map to map view
        mapView.map = map
        
        //add graphics overlay to the map view
        mapView.graphicsOverlays.add(graphicsOverlay)
        
        //touch delegate for map view
        mapView.touchDelegate = self
        
        //intiate Point
      //  displayGraphicAtPoint(point)
       
        
}
    
   
    
    
    private func displayGraphicAtPoint(_ point: AGSPoint) {
           //remove previous graphic from graphics overlay
           graphicsOverlay.graphics.removeAllObjects()
           
           //add graphic at tapped location
           let symbol = AGSSimpleMarkerSymbol(style: .diamond, color: .yellow, size: 20)
           let graphic = AGSGraphic(geometry: point, symbol: symbol, attributes: nil)
           graphicsOverlay.graphics.add(graphic)
       }
    
    
    func geoView(_ geoView: AGSGeoView, didTapAtScreenPoint screenPoint: CGPoint, mapPoint: AGSPoint) {
        // update the point with the tapped location
        point = mapPoint
        label?.text = AGSCoordinateFormatter.latitudeLongitudeString(from: point, format: .decimalDegrees , decimalPlaces: 4)

    }
    
    
    
   
    
    
    

}
