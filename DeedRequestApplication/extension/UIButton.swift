//
//  UIButton.swift
//  DeedRequestApplication
//
//  Created by admin on 2/4/2563 BE.
//  Copyright © 2563 EarthBlooProduction. All rights reserved.
//

import Foundation
import UIKit


extension UIButton {
    
    func pulse(){
        let pulse = CASpringAnimation(keyPath: "transform.scale")
        pulse.duration = 0.5
       pulse.fromValue = 0.95
       pulse.toValue = 1.0
//        pulse.autoreverses = true
//        pulse.repeatCount = 2
//        pulse.initialVelocity = 0.5
//        pulse.damping = 1.0
        layer.add(pulse, forKey: nil)
        
    }
    
    
    //adjust button size based on text size
    func setDynamicFontSize() {
             NotificationCenter.default.addObserver(self, selector: #selector(setButtonDynamicFontSize),
                                                    name: UIContentSizeCategory.didChangeNotification,
                                                    object: nil)
         }
         
         @objc func setButtonDynamicFontSize() {
             Common.setButtonTextSizeDynamic(button: self, textStyle: .callout)
         }
    
    
    
    
    
}



class Common {
    
    class func setButtonTextSizeDynamic(button: UIButton, textStyle: UIFont.TextStyle) {
        button.titleLabel?.font = UIFont.preferredFont(forTextStyle: textStyle)
        button.titleLabel?.adjustsFontForContentSizeCategory = true
    }
    
}

