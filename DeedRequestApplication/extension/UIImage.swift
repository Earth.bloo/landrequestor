//
//  File.swift
//  DeedRequestApplication
//
//  Created by admin on 16/3/2563 BE.
//  Copyright © 2563 EarthBlooProduction. All rights reserved.
//
import UIKit
extension UIImage{
   func getCropRatio() -> CGFloat {
       let widthRatio = CGFloat(self.size.width / self.size.height)
       return widthRatio
   }
}
