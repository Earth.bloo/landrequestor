//
//  UILabel.swift
//  DeedRequestApplication
//
//  Created by admin on 16/4/2563 BE.
//  Copyright © 2563 EarthBlooProduction. All rights reserved.
//

import Foundation
import UIKit



   extension UILabel {

       func setLineSpacing(lineSpacing: CGFloat = -1, lineHeightMultiple: CGFloat = -1) {

           guard let labelText = self.text else { return }

           let paragraphStyle = NSMutableParagraphStyle()
           paragraphStyle.lineSpacing = lineSpacing
           paragraphStyle.lineHeightMultiple = lineHeightMultiple

           let attributedString:NSMutableAttributedString
           if let labelattributedText = self.attributedText {
               attributedString = NSMutableAttributedString(attributedString: labelattributedText)
           } else {
               attributedString = NSMutableAttributedString(string: labelText)
           }

           // Line spacing attribute
        attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))

           self.attributedText = attributedString
       }
   }
