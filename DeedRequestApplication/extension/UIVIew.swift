//
//  File.swift
//  DeedRequestApplication
//
//  Created by admin on 14/3/2563 BE.
//  Copyright © 2563 EarthBlooProduction. All rights reserved.
//

import UIKit

extension UIView {
    
    func dropShadow() {
           self.layer.masksToBounds = false
           self.layer.shadowColor = UIColor.black.cgColor
           self.layer.shadowOpacity = 0.5
           self.layer.shadowOffset = CGSize(width: -1, height: 1)
           self.layer.shadowRadius = 1
           self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
           self.layer.shouldRasterize = true
           self.layer.rasterizationScale = UIScreen.main.scale

       }
    
    func setConstraint(top:NSLayoutYAxisAnchor?,leading:NSLayoutXAxisAnchor?,trailing:NSLayoutXAxisAnchor?,bottom:NSLayoutYAxisAnchor?,padding: UIEdgeInsets = .zero,size:CGSize = .zero){
        translatesAutoresizingMaskIntoConstraints = false
        
        if let top = top {
            topAnchor.constraint(equalTo: top, constant: padding.top).isActive = true
        }
        if let leading = leading {
            leadingAnchor.constraint(equalTo: leading, constant: padding.left).isActive = true
        }
        if let trailing = trailing {
            trailingAnchor.constraint(equalTo: trailing, constant: -padding.right).isActive = true
        }
        if let bottom = bottom {
            bottomAnchor.constraint(equalTo: bottom, constant: -padding.bottom).isActive = true
               }
        if size.width != 0 {
            widthAnchor.constraint(equalToConstant: size.width).isActive = true
        }
        if size.height != 0 {
                   heightAnchor.constraint(equalToConstant: size.height).isActive = true
               }
        
    }
    
    

        func AspectRation(_ ratio: CGFloat) -> NSLayoutConstraint {

            return NSLayoutConstraint(item: self, attribute: .height, relatedBy: .equal, toItem: self, attribute: .width, multiplier: ratio, constant: 0)
        }
    
    
   
    
    fileprivate func Stack(_ axis: NSLayoutConstraint.Axis = .vertical, views:[UIView],spacing: CGFloat = 0, alignment:UIStackView.Alignment = .fill,distribution: UIStackView.Distribution = .fillEqually) -> UIStackView {
        let view = UIStackView(arrangedSubviews: views)
        view.axis = axis
        view.spacing = spacing
        view.alignment = alignment
        view.distribution = distribution  
        return view
        
    }
    
    @discardableResult
    open func VStack(_ views:UIView..., spacing:CGFloat = 0,alignment:UIStackView.Alignment = .fill,distribution: UIStackView.Distribution = .fillEqually) -> UIStackView {
        return Stack(.vertical, views: views, spacing: spacing, alignment: alignment, distribution: distribution)
    }
    @discardableResult
    open func HStack(_ views:UIView..., spacing:CGFloat = 0,alignment:UIStackView.Alignment = .fill,distribution: UIStackView.Distribution = .fillEqually) -> UIStackView {
        return Stack(.horizontal, views: views, spacing: spacing, alignment: alignment, distribution: distribution)
    }
    
    
    
    func setGradientBackground(colorone:UIColor,colortwo:UIColor){
        let gradientlayer = CAGradientLayer()
        gradientlayer.frame = bounds
        gradientlayer.colors = [colorone.cgColor,colortwo.cgColor]
        gradientlayer.locations = [0.0,1.0]
        gradientlayer.startPoint = CGPoint(x:1.0,y:1.0)
        gradientlayer.endPoint = CGPoint(x: 0.0, y: 0.0)
        
        layer.insertSublayer(gradientlayer, at: 0)
    }
}
