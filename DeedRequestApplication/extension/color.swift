//
//  File.swift
//  DeedRequestApplication
//
//  Created by admin on 14/3/2563 BE.
//  Copyright © 2563 EarthBlooProduction. All rights reserved.
//
import UIKit
extension UIColor {
    static func rgb(red:CGFloat,green:CGFloat,blue:CGFloat,alpha:CGFloat) -> UIColor {
        return UIColor(red: red/255,green: green/255,blue: blue/255,alpha:alpha)
    }
}
struct colour{
    static let deepblue = UIColor.rgb(red: 20, green: 60, blue: 152,alpha: 1)
    static let blue = UIColor.rgb(red: 39, green: 109, blue: 171,alpha: 1)
}

