//
//  longViewController.swift
//  DeedRequestApplication
//
//  Created by admin on 14/4/2563 BE.
//  Copyright © 2563 EarthBlooProduction. All rights reserved.
//
import Lottie
import UIKit
import MaterialComponents.MaterialTextFields



class longViewController: UIViewController {
    var animatedView:AnimationView = {
           let view = AnimationView()
           view.animation = Animation.named("walkingman")
           view.contentMode = .scaleAspectFit
           view.loopMode = .loop
        
           view.translatesAutoresizingMaskIntoConstraints = false
           return view
       }()
    
    var textcontroller1:MDCTextInputControllerOutlined!
       var textcontroller2:MDCTextInputControllerOutlined!
       var textcontroller3:MDCTextInputControllerOutlined!
       
       var firstnameTextField:MDCTextField = {
           let view = MDCTextField()
           view.placeholder = "First name"
           view.text = "Peerapat"
           view.font = UIFont(name: "AirbnbCerealApp-Book", size: 20)
           view.translatesAutoresizingMaskIntoConstraints = false
           return view
       }()
       
       var LastnameTextField:MDCTextField = {
           let view = MDCTextField()
           view.placeholder = "Last name"
           view.text = "Dechathitirat"
           view.font = UIFont(name: "AirbnbCerealApp-Book", size: 20)
           view.translatesAutoresizingMaskIntoConstraints = false
           return view
       }()
       
       var PNTextField:MDCTextField = {
           let view = MDCTextField()
           view.placeholder = "Phone Number"
           view.text = "0877588057"
           view.font = UIFont(name: "AirbnbCerealApp-Book", size: 20)
           view.translatesAutoresizingMaskIntoConstraints = false
           return view
       }()
       
       var logoutButton:UIButton = {
           let view = UIButton()
           view.layer.borderWidth = 1
           view.layer.borderColor = UIColor.red.cgColor
           view.setTitle("Logout", for: UIControl.State.normal)
           view.titleLabel?.font = UIFont(name: "AirbnbCerealApp-Light",size: 18)
           view.setTitleColor(.red, for: UIControl.State.normal)
           view.layer.cornerRadius = 20
           view.translatesAutoresizingMaskIntoConstraints = false
           //view.addTarget(self, action: #selector(ClickLogout), for: .touchUpInside)
           return view
       }()

    override func viewDidLoad() {
        super.viewDidLoad()

        setUI()
    }
    
    func setUI(){
        
       
        animatedView.heightAnchor.constraint(equalToConstant: 300).isActive = true
        firstnameTextField.widthAnchor.constraint(equalToConstant: 300).isActive = true
        LastnameTextField.widthAnchor.constraint(equalToConstant: 300).isActive = true
        PNTextField.widthAnchor.constraint(equalToConstant: 300).isActive = true
        logoutButton.widthAnchor.constraint(equalToConstant: 100).isActive = true
        
        
       
       
        
        let stackTextfield = UIStackView(arrangedSubviews:[firstnameTextField,LastnameTextField,PNTextField])
        stackTextfield.distribution = .fillEqually
        stackTextfield.alignment = .center
        stackTextfield.axis = .vertical
        stackTextfield.spacing = 40
       
        
        let stackall = UIStackView(arrangedSubviews: [animatedView,stackTextfield,logoutButton])
        stackall.translatesAutoresizingMaskIntoConstraints = false
        stackall.alignment = .center
        stackall.backgroundColor = .red
        stackall.distribution = .fill
        stackall.axis = .vertical
        stackall.spacing = 40
        view.addSubview(stackall)
        
        
        
        
        stackall.topAnchor.constraint(equalTo: view.topAnchor,constant: 60).isActive = true
        stackall.leadingAnchor.constraint(equalTo: view.leadingAnchor,constant: 20).isActive = true
        stackall.trailingAnchor.constraint(equalTo: view.trailingAnchor,constant: -20).isActive = true
        stackall.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
